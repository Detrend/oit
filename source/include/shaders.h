//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#pragma once

#include "Common.h"

#include <GLAD/glad.h>

namespace oit {

GLuint compile_and_link_gpu_program(cstr vertex_path, cstr fragment_path) noexcept;

}
