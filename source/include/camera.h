//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#pragma once

#include "Common.h"

namespace oit {

struct camera
{
  glm::vec3 position               = glm::zero<glm::vec3>();
  glm::quat rotation               = glm::identity<glm::quat>();
  float     fov_horizontal_degrees = 90.0f;
  float     near                   = 0.1f;
  float     far                    = 1000.0f;
};

glm::mat4 calculate_camera_projection(const camera& camera, f32 aspect_x_div_y);

glm::mat4 calculate_camera_view(const camera& camera);

}
