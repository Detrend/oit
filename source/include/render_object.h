//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#pragma once

#include "Common.h"

#include <GLAD/glad.h>

#include <vector>

namespace oit {

using obj_flags = u8;
namespace object_flags
{
  enum etype : obj_flags
  {
    none        = 0,
    opaque      = 1 << 0, // texture contains at least one fully opaque pixel          (alpha = 1.0)
    transparent = 1 << 1, // texture contains at least one partially transparent pixel (alpha > 0.0 && alpha < 1.0)
    empty       = 1 << 2, // texture contains at least one fully transparent pixel     (alpha = 0.0)
    particle    = 1 << 3, // the object is a particle and should be rotated towards camera

    visible = opaque | transparent | empty,
  };
}

struct render_object
{
  GLuint vertex_buffer_handle = 0;
  GLuint index_buffer_handle  = 0;
  u32    index_count          = 0;
  GLuint texture_handle       = 0;

  glm::vec3 position = glm::vec3{0.0f};
  glm::quat rotation = glm::identity<glm::quat>();
  float     scale    = 1.0f;
  obj_flags flags    = object_flags::none;
};

void deallocate_render_object(render_object& object) noexcept;

glm::mat4 calculate_render_object_transform(const render_object& obj) noexcept;

glm::mat4 calculate_particle_transform(const render_object& particle, glm::quat camera_rotation) noexcept;

enum class scene_type
{
  loaded_mesh,
  particle_cloud,
};

struct render_scene
{
  // name & objects
  std::string                file_path;
  std::vector<render_object> objects;
  scene_type                 type;

  // transform
  glm::vec3 position = glm::zero<glm::vec3>();
  glm::quat rotation = glm::identity<glm::quat>();
  float     scale    = 1.0f;

  // color override
  glm::vec4 color_override = glm::vec4(1.0f);
};

glm::mat4 calculate_render_scene_transform(const render_scene& scene) noexcept;

}

