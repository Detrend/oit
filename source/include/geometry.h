//=============================================================//
// Author: Matus Rozek                                         //
// Year: 2024                                                  //
// Program written as a part of a bachelor thesis for MFF Cuni //
//=============================================================//
#pragma once

#include "Common.h"
#include "render_object.h"
#include <GLAD/glad.h>

#include <vector>
#include <utility>

// forward declarations
struct aiScene;
struct aiNode;
struct aiMesh;

namespace oit {

struct vertex_data
{
  glm::vec3 position;
  glm::vec2 tex_coord;
};

struct geo_mesh
{
  std::vector<vertex_data> vertices;
  std::vector<u32>         indices;
  std::string              texture_path;
};

struct texture_layout
{
  u8 r;
  u8 g;
  u8 b;
  u8 a;
};

class geometry
{
public:
  static void init();

  static void terminate();

  static [[nodiscard]] std::vector<geo_mesh> load_from_file(cstr path) noexcept;

  static [[nodiscard]] std::vector<render_object> load_render_objects_from_model_file(cstr path) noexcept;

  static [[nodiscard]] render_scene create_render_scene_from_model_file(cstr path) noexcept;

  static [[nodiscard]] render_scene create_particle_cloud(u32 particle_count) noexcept;

private:
  static void process_node(aiNode* node, const aiScene* scene, std::vector<geo_mesh>& mesh_output) noexcept;

  static void process_mesh(aiMesh* mesh, const aiScene* scene, std::vector<geo_mesh>& mesh_output) noexcept;

  static obj_flags analyze_texture(texture_layout* data, int width, int height) noexcept;

  static [[nodiscard]] GLuint load_texture(cstr path, obj_flags& flags_out) noexcept;

private:
  constexpr static cstr PARTICLE_TEXTURE_PATH = "models/particle_texture.png";

  inline static GLuint ms_particle_vbo            = 0;
  inline static GLuint ms_particle_ibo            = 0;
  inline static GLuint ms_particle_texture_handle = 0;
  inline static u32    ms_particle_index_count    = 0;
  inline static obj_flags ms_particle_flags       = object_flags::none;
};

}

