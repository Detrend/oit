//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#pragma once

#include "Common.h"
#include "render_object.h"
#include "camera.h"

// transparency algorithms
#include <algorithms/solid_only.h>
#include <algorithms/depth_peeling.h>
#include <algorithms/meshkin.h>
#include <algorithms/weighted_average.h>
#include <algorithms/wboit.h>
#include <algorithms/moment.h>

#include <GLAD/glad.h>
#include <JSON/json_fwd.hpp>

#include <vector>

// Forward declarations
struct SDL_Window;

namespace oit {

namespace oit_methods
{
  enum types
  {
    solid_only = 0,
    depth_peeling,
    weighted_sum,
    weighted_average,
    wboit,
    moment,
    count
  };
}

constexpr cstr OIT_METHOD_NAMES[oit_methods::count]
{
  "solid only",
  "depth peeling",
  "weighted sum",
  "weighted average",
  "wboit",
  "moment",
};

constexpr cstr DEFAULT_SCENE_PATH = "scenes/default_scene.json";

// Basic class for running the demo
class demo_app
{
public:
  // initializes the application
  void init() noexcept;
    
  // runs the app
  void run() noexcept;

  // terminates the app
  void terminate() noexcept;

private:  // UPDATE //
  [[nodiscard]] bool do_one_frame(float seconds)  noexcept;
  void               update(float seconds)        noexcept;
  void               update_camera(float seconds) noexcept;
  void               do_rendering()               noexcept;
  void               render_world() noexcept;

private:  // INITIALIZATION //
  void init_window_and_opengl() noexcept;
  void init_oit_algorithms()    noexcept;
  void init_imgui()             noexcept;

private: // IMGUI INTERFACE //
  void render_interface()             noexcept;
  void render_interface_top_menu()    noexcept;
  void render_interface_scene()       noexcept;
  void render_interface_performance() noexcept;
  void render_interface_camera()      noexcept;

private: // SERIALIZE/DESERIALIZE //
  bool serialize_scene_into_file(cstr file_name)   noexcept;
  bool deserialize_scene_from_file(cstr file_name) noexcept;
  bool serialize_scene(nlohmann::json& json, render_scene& scene, bool deserialize) noexcept;

private: // SIGNALS //
  void on_screen_resize() noexcept;

private: // MISC //
  void cleanup_scene() noexcept;

private:
  constexpr static cstr WINDOW_NAME    = "OIT demo";
  constexpr static u32  WINDOW_SIZE[2] = {800, 600};

private:
  SDL_Window* m_window         = nullptr;
  void*       m_opengl_context = nullptr;

  camera m_camera;

  struct
  {
    algorithms::solid_only::temp_data       solid_data;
    algorithms::meshkin::temp_data          meshkin_data;
    algorithms::depth_peeling::temp_data    dp_data;
    algorithms::weighted_average::temp_data wa_data;
    algorithms::wboit::temp_data            wboit_data;
    algorithms::moment::temp_data           moment_data;
  }
  m_algorithm_data;

  // interface
  struct
  {
    struct
    {
      float speed       = 10.0f;
      float sensitivity = 0.5f;
    }
    camera_settings;

    bool camera_window_visible      = false;
    bool scene_window_visible       = true;

    struct
    {
      float frametime     = 0.0f; // in seconds
      float frametime_sum = 0.0f;;
      int   sum_members   = 0;
      int   last_fps      = 0;
      static constexpr float NUM_SECONDS_AVG = 1.0f;
    }
    performance;
    bool performance_window_visible = false;

    glm::vec4 background_color = glm::vec4{1.0f};
    bool      do_face_culling  = false;
  }
  m_interface_settings;

  glm::mat4 m_camera_projection_cached;

  float m_previous_mouse_position[2]{};

  GLuint m_vao = 0;

  std::vector<render_scene> m_scenes;

  // current transparency rendering method
  oit_methods::types m_transparency_method = oit_methods::solid_only;
};

}

