//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#pragma once

#define GLM_ENABLE_EXPERIMENTAL
#include <GLM/glm.hpp>
#include <GLM/gtc/quaternion.hpp>
#include <GLM/gtx/quaternion.hpp>
#include <GLM/gtc/type_ptr.hpp>

#include <format>                                                 // std::format
#include <iostream>                                               // std::cout
#include <cstdlib>                                                // std::abort
#include <cstdint>                                                // std::uint32

namespace oit {

#ifdef _DEBUG

// Logs out an output to the console
template<typename...types>
void log(const char* msg, types&&...args) noexcept
{
  std::cout << std::vformat(msg, std::make_format_args(args...)) << std::endl;
}

#else

// Logs out an output to the console
template<typename...types>
void log([[maybe_unused]]const char* msg,
         [[maybe_unused]]types&&...args) noexcept {}

#endif

using s8 = std::int8_t;
using u8 = std::uint8_t;

using byte = u8;

using s16 = std::int16_t;
using u16 = std::uint16_t;

using s32 = std::int32_t;
using u32 = std::uint32_t;

using s64 = std::int64_t;
using u64 = std::uint64_t;

using f32 = float;
using f64 = double;

using cstr = const char*;

constexpr glm::vec3 FORWARD = glm::vec3{0.0f, 0.0f, -1.0f};
constexpr glm::vec3 RIGHT   = glm::vec3{1.0f, 0.0f,  0.0f};
constexpr glm::vec3 UP      = glm::vec3{0.0f, 1.0f,  0.0f};

}

#ifdef _DEBUG

template<typename v>
void OIT_ASSERT(v&& val)
{
  if (!(val))
  {
    std::abort();
  }
}

template<typename v, typename...types>
void OIT_ASSERT(v&& val, const char* msg, types&&...args)
{
  if (!(val))
  {
    oit::log(msg, std::forward<types>(args)...);
    std::abort();
  }
}

#else

template<typename v>
void OIT_ASSERT([[maybe_unused]]v&& val)
{
}

template<typename v, typename...types>
void OIT_ASSERT([[maybe_unused]]v&& val,
                [[maybe_unused]]const char* msg,
                [[maybe_unused]]types&&...args)
{
}

#endif

