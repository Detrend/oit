//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#pragma once

#include <algorithms\common.h>

namespace oit::algorithms::moment {

namespace uniforms
{
  enum etype
  {
    moments_sum_sampler  = 43,
    moments_sampler      = 44,
    near_far             = 71,
  };
}

constexpr const char* SOLID_SHADERS [] = {"shaders/solid.vert",            "shaders/solid.frag"};
constexpr const char* MOMENT_SHADERS[] = {"shaders/solid.vert",            "shaders/moment_moments.frag"};
constexpr const char* ACCUM_SHADERS [] = {"shaders/solid.vert",            "shaders/moment_accum.frag"};
constexpr const char* OIT_SHADERS   [] = {"shaders/full_screen_quad.vert", "shaders/bav_gui_oit.frag"};

struct temp_data : common::temp_data_base
{
  GLuint solid_gpu_program;
  GLuint moment_gpu_program;
  GLuint accumulate_gpu_program;
  GLuint oit_gpu_program;

  common::framebuffer             f1;  // solid pass
  common::accumulator_framebuffer f2;  // moments and moments sum
  common::accumulator_framebuffer f3;  // sum of colors & alpha, coverage

  bool f16_accumulators  = false;
};

temp_data init(int width, int height) noexcept;

void on_resize_screen(temp_data& temp, int width, int height) noexcept;

void draw_interface(temp_data& temp) noexcept;

void terminate(temp_data& temp) noexcept;

void render_world(temp_data& temp, const common::render_data& data, std::span<oit::render_scene> scenes) noexcept;

}

