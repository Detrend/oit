//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#pragma once

#include "render_object.h"    // render_flags

#include <GLAD\glad.h>

#include <span>

namespace oit {
struct render_data;
struct camera;
}

namespace oit::algorithms::common {

// normal framebuffer
struct framebuffer
{
  GLuint frame_buffer_handle    = 0;
  GLuint color_component_handle = 0;
  GLuint depth_component_handle = 0;
};

// accumulator framebuffer (2 color components - quad with 4 channels and single with one channel)
struct accumulator_framebuffer
{
  GLuint frame_buffer_handle     = 0;
  GLuint depth_component_handle  = 0;
  GLuint quad_component_handle   = 0;
  GLuint single_component_handle = 0;
};

struct accumplus_framebuffer
{
  GLuint frame_buffer_handle    = 0;
  GLuint depth_component_handle = 0;
  GLuint color_components[8]    = {};
  u8     num_color_components   = 0;
};

struct temp_data_base
{
  int viewport_width  = 0;
  int viewport_height = 0;
};

struct render_data
{
  oit::camera& camera;
  glm::mat4&   projection_matrix;
  glm::vec4    background_color;
};

// using namespaced enum instead of enum class for implicit conversions
// instead of static_casting
namespace uniforms
{
  enum etype
  {
    local_to_world  = 0,
    world_to_view   = 1,
    view_to_screen  = 2,
    local_to_screen = 3,

    texture_sampler = 42,

    color_override = 69,
  };
}

void delete_framebuffer(common::framebuffer& f);
void generate_framebuffer(common::framebuffer& data, int width, int height, bool renderbuffer = true, GLint int_format = GL_RGBA);

void generate_framebuffer(
  common::accumplus_framebuffer& data,
  int                            width,
  int                            height,
  u8                             num_color_components,
  GLenum                         color_format,
  bool                           renderbuffer = true);
void delete_framebuffer(common::accumplus_framebuffer& data);

void render_scene(
  const render_data&           data,
  std::span<oit::render_scene> scenes,
  obj_flags                    flags_present = object_flags::opaque) noexcept;

void clear_color_and_depth(glm::vec4 background_color);

void delete_accum(accumulator_framebuffer& f);

void generate_accum(
  accumulator_framebuffer& data,
  int                      width,
  int                      height,
  GLint                    single_format = GL_R32F,     // GL_R32F,    GL_R16F,    GL_R8
  GLint                    quad_format   = GL_RGBA32F); // GL_RGBA32F, GL_RGBA16F, GL_RGBA8

void blit(GLuint from, GLuint to, GLbitfield mask, int width, int height);

}
