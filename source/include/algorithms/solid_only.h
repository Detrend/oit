//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#pragma once

#include <algorithms\common.h>

namespace oit::algorithms::solid_only {

struct temp_data
{
  static constexpr const char* SHADERS[] = {"shaders/solid.vert", "shaders/solid_only.frag"};
  obj_flags render_flags = object_flags::visible;
  GLuint    gpu_program;
  bool      hardware_blend = false;
};

temp_data init() noexcept;

void terminate(temp_data& temp) noexcept;

void draw_interface(temp_data& temp) noexcept;

void render_world(temp_data& temp, const common::render_data& data, std::span<oit::render_scene> scenes) noexcept;

}

