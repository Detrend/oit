//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#pragma once

#include <algorithms/common.h>
#include <common.h>

namespace oit {
struct render_scene;
}

namespace oit::algorithms::depth_peeling {

namespace uniforms
{
  enum etype
  {
    additional_depth  = 51,
    transparent_layer = 99,
  };
}

constexpr cstr SOLID_SHADERS[] = {"shaders/solid.vert",            "shaders/solid.frag"};
constexpr cstr PEEL_SHADERS [] = {"shaders/solid.vert",            "shaders/peel.frag"};
constexpr cstr MERGE_SHADERS[] = {"shaders/full_screen_quad.vert", "shaders/peel_merge.frag"};

struct temp_data : common::temp_data_base
{
  GLuint solid_gpu_program = 0;
  GLuint peel_gpu_program  = 0;
  GLuint merge_gpu_program = 0;

  common::framebuffer f1{}; // solid background + depth
  common::framebuffer f2{}; // render transparent stuff here
  common::framebuffer f3{}; // and merge it into here.. (keep the other depth here)

  u32 num_passes = 5;
};

temp_data init(int width, int height) noexcept;

void on_resize_screen(temp_data& temp, int width, int height) noexcept;

void draw_interface(temp_data& temp) noexcept;

void terminate(temp_data& temp) noexcept;

void render_world(temp_data& temp, const common::render_data& data, std::span<render_scene> scenes) noexcept;

}
