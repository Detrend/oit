//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#pragma once

#include <algorithms\common.h>

namespace oit::algorithms::meshkin {

namespace uniforms
{
  enum etype
  {
    object_texture      = 42,
    accumulator_texture = 43,
    background_texture  = 44,
  };
}

constexpr cstr SOLID_SHADERS[] = {"shaders/solid.vert",            "shaders/solid.frag"};
constexpr cstr ACCUM_SHADERS[] = {"shaders/solid.vert",            "shaders/meshkin_accum.frag"};
constexpr cstr OIT_SHADERS  [] = {"shaders/full_screen_quad.vert", "shaders/meshkin_oit.frag"};

struct temp_data : common::temp_data_base
{
  // gpu programs
  GLuint solid_gpu_program = 0;
  GLuint accum_gpu_program = 0;
  GLuint oit_gpu_program   = 0;

  common::framebuffer f1{}; // background
  common::framebuffer f2{}; // accumulator (RGBA)

  // settings
  bool draw_debug = false;
};

temp_data init(int width, int height) noexcept;

void on_resize_screen(temp_data& temp, int width, int height) noexcept;

void draw_interface(temp_data& temp) noexcept;

void terminate(temp_data& temp) noexcept;

void render_world(temp_data& temp, const common::render_data& data, std::span<render_scene> scenes) noexcept;

}
