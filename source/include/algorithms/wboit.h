//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#pragma once

#include <common.h>               // cstr
#include <algorithms\common.h>

namespace oit {
struct render_scene;
}

namespace oit::algorithms::wboit {

namespace uniforms
{
  enum etype
  {
    weight_method  = 70,
    near_far       = 71,
    scale_near_far = 80,
    depth_sampler  = 99,
  };
}

namespace weight_function
{
  enum etype
  {
    none = 0,
    z_m3,
    z_m5,
    z_m3a,
    z_m5a,
    proposed1,
    proposed2,
    proposed3,
    adaptive_z_m3,
    adaptive_z_m5,
    adaptive_z_m3a,
    adaptive_z_m5a,
    adaptive_z_m3a2,
    adaptive_z_m5a2,
    count
  };
}


namespace accum_accuracy
{
  enum etype
  { 
    F32,
    F16,
    count
  };
}

constexpr cstr ACCUM_ACCURACY_STR[accum_accuracy::count]
{
  "F32",
  "F16",
};

constexpr cstr WEIGHT_FUNCTIONS_STR[weight_function::count]
{
  "none",
  "z^-3",
  "z^-5",
  "z^-3 * alpha",
  "z^-5 * alpha",
  "proposed1",
  "proposed2",
  "proposed3",
  "adaptive z^-3",
  "adaptive z^-5",
  "adaptive z^-3 * alpha",
  "adaptive z^-5 * alpha",
  "adaptive z^-3 * alpha^2",
  "adaptive z^-5 * alpha^2",
};

constexpr cstr SOLID_SHADERS[] = {"shaders/solid.vert",            "shaders/solid.frag"};
constexpr cstr ACCUM_SHADERS[] = {"shaders/solid.vert",            "shaders/bav_gui_accum.frag"};
constexpr cstr OIT_SHADERS[]   = {"shaders/full_screen_quad.vert", "shaders/bav_gui_oit.frag"};

struct temp_data : common::temp_data_base
{
  GLuint solid_gpu_program;
  GLuint accumulate_gpu_program;
  GLuint oit_gpu_program;

  common::framebuffer             f1;  // solid pass with depth buffer instead of renderbuffer
  common::accumulator_framebuffer f2;  // transparent pass 1

  float                  scale_near = 0.0f;
  float                  scale_far  = 300.0f;
  weight_function::etype weights    = weight_function::none;
  accum_accuracy::etype  accuracy   = accum_accuracy::F32;
};

temp_data init(int width, int height) noexcept;

void on_resize_screen(temp_data& temp, int width, int height) noexcept;

void draw_interface(temp_data& temp) noexcept;

void terminate(temp_data& temp) noexcept;

void render_world(temp_data& temp, const common::render_data& data, std::span<render_scene> scenes) noexcept;

}
