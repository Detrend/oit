//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#pragma once

#include <algorithms\common.h>

namespace oit::algorithms::weighted_average {

struct temp_data : common::temp_data_base
{
  static constexpr const char* SOLID_SHADERS[] = {"shaders/solid.vert",            "shaders/solid.frag"};
  static constexpr const char* ACCUM_SHADERS[] = {"shaders/solid.vert",            "shaders/bav_myers_accum.frag"};
  static constexpr const char* OIT_SHADERS[]   = {"shaders/full_screen_quad.vert", "shaders/bav_myers_oit.frag"};

  GLuint solid_gpu_program;
  GLuint accumulate_gpu_program;
  GLuint oit_gpu_program;

  common::framebuffer             f1; // solid pass
  common::accumulator_framebuffer f2; // transparent pass 1

  bool f16_precision = false;
};

temp_data init(int width, int height) noexcept;

void on_resize_screen(temp_data& temp, int width, int height) noexcept;

void draw_interface(temp_data& temp) noexcept;

void terminate(temp_data& temp) noexcept;

void render_world(temp_data& temp, const common::render_data& data, std::span<oit::render_scene> scenes) noexcept;

}

