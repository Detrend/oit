//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#include "shaders.h"

#include <GLAD/glad.h>

#include <string>
#include <fstream>

namespace oit {

constexpr u32 SHADER_INFO_LENGTH = 512;

//============================================================================//
static bool file_to_string(cstr file_path, std::string& output) noexcept
{
  std::ifstream file(file_path, std::ios::in);
  if (!file.is_open())
  {
    return false;
  }

  std::string line;
  while (std::getline(file, line))
  {
    output += line + "\n";
  }

  return true;
}

//============================================================================//
static GLuint compile_shader_from_source(cstr source, u32 shader_type)
{
  OIT_ASSERT(source);
  OIT_ASSERT(shader_type == GL_VERTEX_SHADER || shader_type == GL_FRAGMENT_SHADER);

  GLuint shader_handle;
  shader_handle = glCreateShader(shader_type);
  glShaderSource(shader_handle, 1, &source, nullptr);
  glCompileShader(shader_handle);

  int success = 0;
  glGetShaderiv(shader_handle, GL_COMPILE_STATUS, &success);
  if (!success)
  {
    char info[SHADER_INFO_LENGTH]{};
    glGetShaderInfoLog(shader_handle, SHADER_INFO_LENGTH, nullptr, info);
    log("[Error] Failed to compile shader from source. Reason: {}", info);
    OIT_ASSERT(false);
    return 0;
  }

  return shader_handle;
}

//============================================================================//
GLuint compile_and_link_gpu_program(cstr vertex_path, cstr fragment_path) noexcept
{
  std::string vertex_source, fragment_source;
  bool ok = true;

  ok = file_to_string(vertex_path, vertex_source);
  OIT_ASSERT(ok);

  ok = file_to_string(fragment_path, fragment_source);
  OIT_ASSERT(ok);

  auto vertex_handle   = compile_shader_from_source(vertex_source.c_str(), GL_VERTEX_SHADER);
  auto fragment_handle = compile_shader_from_source(fragment_source.c_str(), GL_FRAGMENT_SHADER);

  GLuint program_handle = glCreateProgram();
  glAttachShader(program_handle, vertex_handle);
  glAttachShader(program_handle, fragment_handle);
  glLinkProgram(program_handle);

  glDeleteShader(vertex_handle);
  glDeleteShader(fragment_handle);

  int success = 0;
  glGetProgramiv(program_handle, GL_LINK_STATUS, &success);
  if(!success)
  {
    char info[SHADER_INFO_LENGTH]{};
    glGetProgramInfoLog(program_handle, SHADER_INFO_LENGTH, NULL, info);
    log("[Error] Failed to link gpu program. Reason: {}", info);
    OIT_ASSERT(false);
  }

  return program_handle;
}

}
