//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#include "render_object.h"

#include <glad/glad.h>

namespace oit {

//============================================================================//
void deallocate_render_object(render_object& object) noexcept
{
  if (object.flags & object_flags::particle)
  {
    // we do not deallocate particles, because all
    // particles share common resources that are allocated
    // and deallocated with the program lifetime
    return;
  }

  // delete vertices
  if (object.vertex_buffer_handle)
  {
    glDeleteBuffers(1, &object.vertex_buffer_handle);
    object.vertex_buffer_handle = 0;
  }

  // delete indices
  if (object.index_buffer_handle)
  {
    glDeleteBuffers(1, &object.index_buffer_handle);
    object.index_buffer_handle = 0;
  }

  // delete texture
  if (object.texture_handle)
  {
    glDeleteTextures(1, &object.texture_handle);
    object.texture_handle = 0;
  }
}

//============================================================================//
glm::mat4 calculate_render_object_transform(const render_object& obj) noexcept
{
  auto scale = glm::mat4{1.0f} * obj.scale;
  scale[3][3] = 1.0f;
  auto rotate = glm::toMat4(obj.rotation);
  auto translate = glm::translate(glm::mat4{1.0f}, obj.position);
  return translate * rotate * scale;
}

//============================================================================//
glm::mat4 calculate_particle_transform(const render_object& particle, glm::quat camera_rotation) noexcept
{
  auto scale = glm::mat4{1.0f} * particle.scale;
  scale[3][3] = 1.0f;
  auto rotate = glm::toMat4(camera_rotation);
  auto translate = glm::translate(glm::mat4{1.0f}, particle.position);
  return translate * rotate * scale;
}

//============================================================================//
glm::mat4 calculate_render_scene_transform(const render_scene& scene) noexcept
{
  auto scale = glm::mat4{1.0f} * scene.scale;
  scale[3][3] = 1.0f;
  auto rotate = glm::toMat4(scene.rotation);
  auto translate = glm::translate(glm::mat4{1.0f}, scene.position);
  return translate * rotate * scale;
}

}
