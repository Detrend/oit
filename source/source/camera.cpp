//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//

#include "camera.h"

namespace oit {

//============================================================================//
glm::mat4 calculate_camera_projection(const camera& camera, f32 aspect)
{
  return glm::perspective(
    glm::radians(camera.fov_horizontal_degrees / aspect),
    aspect,
    camera.near,
    camera.far);
}

//============================================================================//
glm::mat4 calculate_camera_view(const camera& camera)
{
  return glm::lookAt(
    camera.position,
    camera.position + camera.rotation * FORWARD,
    camera.rotation * UP);
}

}
