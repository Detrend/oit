//=============================================================//
// Author: Matus Rozek                                         //
// Year: 2024                                                  //
// Program written as a part of a bachelor thesis for MFF Cuni //
//=============================================================//

#include "geometry.h"

#include "Common.h"

#include <filesystem>
#include <format>
#include <iterator>
#include <cstdlib> // std::rand

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include <STB/stb_image.h>

namespace oit {

//============================================================================//
/*static*/ void geometry::process_node(aiNode* node, const aiScene* scene, std::vector<geo_mesh>& mesh_output) noexcept
{
  if (node == nullptr) {
    return;
  }

  for (std::size_t i = 0; i < node->mNumMeshes; ++i) {
    process_mesh(scene->mMeshes[node->mMeshes[i]], scene, mesh_output);
  }

  for (std::size_t i = 0; i < node->mNumChildren; ++i) {
    process_node(node->mChildren[i], scene, mesh_output);
  }
}

//============================================================================//
/*static*/ void geometry::process_mesh(aiMesh* mesh, const aiScene* scene, std::vector<geo_mesh>& mesh_output) noexcept
{
  if (mesh == nullptr)
    return;

  geo_mesh ret;

  auto materialIndex = mesh->mMaterialIndex;
  auto* material = scene->mMaterials[materialIndex];

  aiString matName;
  material->Get(AI_MATKEY_NAME, matName);

  constexpr aiTextureType TEXTURE_LOADING_ORDER[]
  {
    aiTextureType_DIFFUSE,
    aiTextureType_BASE_COLOR,
    aiTextureType_UNKNOWN,
    aiTextureType_NONE,
  };

  for (int i = 0; i <= std::size(TEXTURE_LOADING_ORDER); ++i)
  {
    auto type = TEXTURE_LOADING_ORDER[i];
    if (aiString texPath; material->GetTextureCount(type) > 0)
    {
      material->GetTexture(type, 0, &texPath);
      ret.texture_path = std::string(texPath.C_Str(), texPath.C_Str() + texPath.length - 4);
      break;
    }
  }

  if (ret.texture_path.empty())
  {
    ret.texture_path = "<invalid>";
    log("[Warning] failed to load texture from model");
  }

  for (std::size_t i = 0; i < mesh->mNumVertices; ++i)
  {
    vertex_data vertex;
    vertex.position.x = mesh->mVertices[i].x;
    vertex.position.y = mesh->mVertices[i].y;
    vertex.position.z = mesh->mVertices[i].z;

    OIT_ASSERT(mesh->mTextureCoords[0]);
    vertex.tex_coord.x = mesh->mTextureCoords[0][i].x; 
    vertex.tex_coord.y = mesh->mTextureCoords[0][i].y;

    ret.vertices.push_back(vertex);
  }

  for (std::size_t i = 0; i < mesh->mNumFaces; ++i)
  {
    std::size_t indices = mesh->mFaces[i].mNumIndices;
    OIT_ASSERT(indices == 3);
    for (std::size_t j = 0; j < 3; ++j)
    {
      ret.indices.push_back(mesh->mFaces[i].mIndices[j]);
    }
  }

  mesh_output.emplace_back(std::move(ret));
}

//============================================================================//
obj_flags geometry::analyze_texture(texture_layout* data, int width, int height) noexcept
{
  obj_flags flags = 0;
  OIT_ASSERT(data);

  for (int i = 0; i < height; ++i)
  {
    for (int j = 0; j < width; ++j)
    {
      switch (data[i * height + j].a)
      {
        case 0:   flags |= object_flags::empty;       break;
        case 255: flags |= object_flags::opaque;      break;
        default:  flags |= object_flags::transparent; break;
      }

      if (flags & object_flags::empty
       && flags & object_flags::opaque
       && flags & object_flags::transparent)
      {
        return flags;
      }
    }
  }

  return flags;
}

//============================================================================//
/*static*/ [[nodiscard]] GLuint geometry::load_texture(cstr path, obj_flags& flags_out) noexcept
{
  constexpr int DESIRED_NUM_CHANNELS = 4;
  int width, height, channels;

  byte* texture_data = stbi_load(
    path,
    &width,
    &height,
    &channels,
    DESIRED_NUM_CHANNELS);

  GLuint texture_handle = 0;

  if (texture_data)
  {
    flags_out = geometry::analyze_texture(
      reinterpret_cast<texture_layout*>(texture_data), width, height);

    glGenTextures(1, &texture_handle);
    glBindTexture(GL_TEXTURE_2D, texture_handle);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_data);
    glGenerateMipmap(GL_TEXTURE_2D);

    stbi_image_free(texture_data);
  }

  return texture_handle;
}

//============================================================================//
/*static*/ std::vector<render_object> geometry::load_render_objects_from_model_file(cstr path) noexcept
{
  std::vector<render_object> render_objects;

  auto geo_meshes = load_from_file(path);

  auto directory_path = std::filesystem::path(path).remove_filename();

  for (const auto&[vertices, indices, texture_path] : geo_meshes)
  {
    enum buffer_type
    {
      vertex_buffer = 0,
      index_buffer
    };

    GLuint buffers[2];
    glGenBuffers(2, buffers);

    // vertices
    glBindBuffer(GL_ARRAY_BUFFER, buffers[buffer_type::vertex_buffer]);
    glBufferData(
      GL_ARRAY_BUFFER,
      sizeof(vertex_data) * vertices.size(),
      vertices.data(),
      GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // indices
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[buffer_type::index_buffer]);
    glBufferData(
      GL_ELEMENT_ARRAY_BUFFER,
      sizeof(u32) * indices.size(),
      indices.data(),
      GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // texture
    GLuint    texture_handle = 0;
    obj_flags texture_flags  = 0;
    if (!texture_path.empty())
    {
      // we support only PNG textures
      std::string adjusted_path = directory_path.string() + texture_path + ".png";

      constexpr int DESIRED_NUM_CHANNELS = 4;
      int width, height, channels;
      byte* texture_data = stbi_load(
        adjusted_path.c_str(),
        &width,
        &height,
        &channels,
        DESIRED_NUM_CHANNELS);

      if (texture_data)
      {
        texture_flags = geometry::analyze_texture(
          reinterpret_cast<texture_layout*>(texture_data), width, height);

        glGenTextures(1, &texture_handle);
        glBindTexture(GL_TEXTURE_2D, texture_handle);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_data);
        glGenerateMipmap(GL_TEXTURE_2D);

        stbi_image_free(texture_data);
      }
    }

    // push it back
    render_objects.push_back(render_object
    {
      .vertex_buffer_handle = buffers[buffer_type::vertex_buffer],
      .index_buffer_handle  = buffers[buffer_type::index_buffer],
      .index_count          = static_cast<u32>(indices.size()),
      .texture_handle       = texture_handle,
      .flags                = texture_flags,
    });
  }

  return render_objects;
}

//============================================================================//
/*static*/ [[nodiscard]] render_scene geometry::create_render_scene_from_model_file(
  cstr path) noexcept
{
  auto objects = load_render_objects_from_model_file(path);
  return render_scene
  {
    .file_path = std::string{path},
    .objects   = std::move(objects),
    .type      = scene_type::loaded_mesh,
  };
}

//============================================================================//
static float generate_unit_point()
{
  float x = static_cast<float>(std::rand()) / RAND_MAX;
  float sign = (std::rand() % 2 == 0) ? 1.0f : -1.0f;
  return x * sign;
}

//============================================================================//
static glm::vec3 generate_point_in_unit_cube()
{
  glm::vec3 p;
  p.x = generate_unit_point();
  p.y = generate_unit_point();
  p.z = generate_unit_point();
  return p;
}

//============================================================================//
/*static*/ [[nodiscard]] render_scene geometry::create_particle_cloud(
  u32       particle_count) noexcept
{
  render_scene scene{};
  scene.file_path = std::format("particle cloud [{}x]", particle_count);
  scene.type = scene_type::particle_cloud;

  for (u32 i = 0; i < particle_count; ++i)
  {
    render_object particle;
    particle.flags = ms_particle_flags;

    particle.vertex_buffer_handle = ms_particle_vbo;
    particle.index_buffer_handle  = ms_particle_ibo;
    particle.index_count          = ms_particle_index_count;
    particle.texture_handle       = ms_particle_texture_handle;

    particle.position = generate_point_in_unit_cube();

    scene.objects.push_back(particle);
  }

  return scene;
}

//============================================================================//
void geometry::init()
{
  constexpr vertex_data PARTICLE_POINTS[]
  {
    vertex_data{{-1.0f, -1.0f, 0.0f}, {0.0f, 0.0f}},
    vertex_data{{ 1.0f, -1.0f, 0.0f}, {1.0f, 0.0f}},
    vertex_data{{ 1.0f,  1.0f, 0.0f}, {1.0f, 1.0f}},
    vertex_data{{-1.0f,  1.0f, 0.0f}, {0.0f, 1.0f}},
  };

  constexpr u32 PARTICLE_VERTEX_COUNT = 
    sizeof(PARTICLE_POINTS) / sizeof(PARTICLE_POINTS[0]);

  constexpr u32 PARTICLE_INDICES[]
  {
    0, 1, 2,
    0, 2, 3,
  };

  constexpr u32 PARTICLE_INDEX_COUNT
    = sizeof(PARTICLE_INDICES) / sizeof(PARTICLE_INDICES[0]);

  ms_particle_index_count = PARTICLE_INDEX_COUNT;

  glGenBuffers(1, &ms_particle_vbo);
  glBindBuffer(GL_ARRAY_BUFFER, ms_particle_vbo);
  glBufferData(
    GL_ARRAY_BUFFER,
    sizeof(vertex_data) * PARTICLE_VERTEX_COUNT,
    PARTICLE_POINTS,
    GL_STATIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glGenBuffers(1, &ms_particle_ibo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ms_particle_ibo);
  glBufferData(
    GL_ELEMENT_ARRAY_BUFFER,
    sizeof(u32) * PARTICLE_INDEX_COUNT,
    PARTICLE_INDICES,
    GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

  ms_particle_texture_handle = geometry::load_texture(PARTICLE_TEXTURE_PATH, ms_particle_flags);
  ms_particle_flags |= object_flags::particle;
}

//============================================================================//
void geometry::terminate()
{
  if (ms_particle_texture_handle)
  {
    glDeleteTextures(1, &ms_particle_texture_handle);
    ms_particle_texture_handle = 0;
  }

  if (ms_particle_vbo)
  {
    glDeleteBuffers(1, &ms_particle_vbo);
    ms_particle_vbo = 0;
  }

  if (ms_particle_ibo)
  {
    glDeleteBuffers(1, &ms_particle_ibo);
    ms_particle_ibo = 0;
  }
}

//============================================================================//
/*static*/ [[nodiscard]] std::vector<geo_mesh> geometry::load_from_file(cstr path) noexcept
{
  Assimp::Importer importer;
  const aiScene* scene = importer.ReadFile(path, 
                            aiProcess_Triangulate
                          | aiProcess_MakeLeftHanded
                          | aiProcess_RemoveComponent
                          | aiProcess_GenUVCoords
                          | aiProcess_FlipWindingOrder);

  OIT_ASSERT(scene);

  std::vector<geo_mesh> meshes;
  process_node(scene->mRootNode, scene, meshes);

  return meshes;
}

}
