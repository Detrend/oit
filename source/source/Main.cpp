//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#include "demo_app.h"

int main()
{
  auto* app = new oit::demo_app();

  app->init();
  app->run();
  app->terminate();

  delete app;
  return 0;
}