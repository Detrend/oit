//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#include "demo_app.h"
#include "common.h"
#include "shaders.h"
#include "Geometry.h"

#include <SDL3/SDL.h>
#include <glad/glad.h>
#include <SDL3/SDL_opengl.h>
#include <SDL3/SDL_opengl_glext.h>

// ImGui
#include <IMGUI/imgui.h>
#include <IMGUI/backends/imgui_impl_sdl3.h>
#include <IMGUI/backends/imgui_impl_opengl3.h>

#include <TNF/tinyfiledialogs.h>

#include <JSON/json.hpp>

#include <chrono>
#include <array>
#include <fstream>

namespace oit {

//============================================================================//
#ifdef OIT_OPENGL_DEBUG
static void GLAPIENTRY opengl_message_callback(
  GLenum        source,
  GLenum        type,
  GLuint        id,
  GLenum        severity,
  GLsizei     /*lenght*/,
  const GLchar* message,
  const void*  /*user*/)
{
  if (severity == GL_DEBUG_SEVERITY_NOTIFICATION)
  {
    return;
  }

  #define OIT_SWITCH_STR(name, value) \
  case value: \
  name = #value; \
  break;

  const char* sourceStr;
  const char* typeStr;
  const char* severityStr;

  switch (source)
  {
    OIT_SWITCH_STR(sourceStr, GL_DEBUG_SOURCE_API);
    OIT_SWITCH_STR(sourceStr, GL_DEBUG_SOURCE_WINDOW_SYSTEM);
    OIT_SWITCH_STR(sourceStr, GL_DEBUG_SOURCE_SHADER_COMPILER);
    OIT_SWITCH_STR(sourceStr, GL_DEBUG_SOURCE_THIRD_PARTY);
    OIT_SWITCH_STR(sourceStr, GL_DEBUG_SOURCE_APPLICATION);
    OIT_SWITCH_STR(sourceStr, GL_DEBUG_SOURCE_OTHER);
    default: sourceStr = "UNKNOWN"; break;
  }

  switch (type)
  {
    OIT_SWITCH_STR(typeStr, GL_DEBUG_TYPE_ERROR);
    OIT_SWITCH_STR(typeStr, GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR);
    OIT_SWITCH_STR(typeStr, GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR);
    OIT_SWITCH_STR(typeStr, GL_DEBUG_TYPE_PORTABILITY);
    OIT_SWITCH_STR(typeStr, GL_DEBUG_TYPE_PERFORMANCE);
    OIT_SWITCH_STR(typeStr, GL_DEBUG_TYPE_OTHER);
    OIT_SWITCH_STR(typeStr, GL_DEBUG_TYPE_MARKER);
    default: typeStr = "UNKNOWN"; break;
  }

  switch (severity)
  {
    OIT_SWITCH_STR(severityStr, GL_DEBUG_SEVERITY_HIGH);
    OIT_SWITCH_STR(severityStr, GL_DEBUG_SEVERITY_MEDIUM);
    OIT_SWITCH_STR(severityStr, GL_DEBUG_SEVERITY_LOW);
    OIT_SWITCH_STR(severityStr, GL_DEBUG_SEVERITY_NOTIFICATION);
    default: severityStr = "UNKNOWN"; break;
  }

  log("[OpenGL] [{}] [{}] [{}] \"{}\". ID: {}", sourceStr, typeStr, severityStr, message, id);

  #undef OIT_SWITCH_STR
}
#endif

//============================================================================//
void demo_app::init() noexcept
{
  log("[Info] Initializing demo application...");

  this->init_window_and_opengl();
  this->init_oit_algorithms();
  this->init_imgui();

  geometry::init();

  if (!this->deserialize_scene_from_file(DEFAULT_SCENE_PATH))
  {
    log("[warning] Could not load a default scene from file {}", DEFAULT_SCENE_PATH);
  }
}

//============================================================================//
void demo_app::run() noexcept
{
  namespace sch = std::chrono;

  log("[Info] Running demo...");

  auto previous = sch::high_resolution_clock::now();

  while (true)
  {
    auto now = sch::high_resolution_clock::now();
    auto delta = now - previous;

    float seconds = sch::duration_cast<sch::microseconds>(delta).count() * 0.000'001f;
    previous = now;

    auto should_continue = this->do_one_frame(seconds);
    if (!should_continue)
    {
      break;
    }
  }
}

//============================================================================//
void demo_app::terminate() noexcept
{
  log("[Info] Terminating demo...");

  // delete render objects from all scenes
  this->cleanup_scene();

  // terminate algorithms
  algorithms::solid_only::terminate(m_algorithm_data.solid_data);    // terminate solid
  algorithms::depth_peeling::terminate(m_algorithm_data.dp_data);    // terminate depth peeling
  algorithms::meshkin::terminate(m_algorithm_data.meshkin_data);     // terminate meshkin
  algorithms::weighted_average::terminate(m_algorithm_data.wa_data); // terminate bav & myers
  algorithms::wboit::terminate(m_algorithm_data.wboit_data);         // terminate bav & mcgui
  algorithms::moment::terminate(m_algorithm_data.moment_data);       // terminate moment oit

  geometry::terminate();

  // delete the one vertex array we have
  glDeleteVertexArrays(1, &m_vao);

  // terminate imgui
  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplSDL3_Shutdown();
  ImGui::DestroyContext();

  // terminate SDL
  SDL_GL_DeleteContext(m_opengl_context);
  SDL_DestroyWindow(m_window);
  SDL_Quit();
}

//============================================================================//
bool demo_app::do_one_frame(float seconds) noexcept
{
  SDL_Event evnt;

  while (SDL_PollEvent(&evnt))
  {
    ImGui_ImplSDL3_ProcessEvent(&evnt);

    switch (evnt.type)
    {
      case SDL_EVENT_QUIT:
      {
        return false;
      }

      // ESC to quit
      case SDL_EVENT_KEY_DOWN:
      {
        if (evnt.key.keysym.sym == SDLK_ESCAPE)
        {
          return false;
        }
        break;
      }

      case SDL_EVENT_WINDOW_RESIZED:
      {
        this->on_screen_resize();
        break;
      }

      default:
      break;
    }
  }

  ImGui_ImplOpenGL3_NewFrame();
  ImGui_ImplSDL3_NewFrame();
  ImGui::NewFrame();

  // update the camera and settings
  this->update(seconds);

  // render stuff..
  this->do_rendering();

  ImGui::Render();
  ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

  // swap buffers
  SDL_GL_SwapWindow(m_window);

  return true;
}

//============================================================================//
void demo_app::update([[maybe_unused]]float seconds) noexcept
{
  this->update_camera(seconds);

  m_interface_settings.performance.frametime = seconds;

  if (ImGui::IsKeyPressed(ImGuiKey_F1))
  {
    m_transparency_method = oit::oit_methods::solid_only;
  }

  if (ImGui::IsKeyPressed(ImGuiKey_F2))
  {
    m_transparency_method = oit::oit_methods::depth_peeling;
  }

  if (ImGui::IsKeyPressed(ImGuiKey_F3))
  {
    m_transparency_method = oit::oit_methods::weighted_sum;
  }

  if (ImGui::IsKeyPressed(ImGuiKey_F4))
  {
    m_transparency_method = oit::oit_methods::weighted_average;
  }

  if (ImGui::IsKeyPressed(ImGuiKey_F5))
  {
    m_transparency_method = oit::oit_methods::wboit;
  }

  if (ImGui::IsKeyPressed(ImGuiKey_F6))
  {
    m_transparency_method = oit::oit_methods::moment;
  }
}

//============================================================================//
void demo_app::update_camera(float seconds) noexcept
{
  // update the camera position
  {
    const bool left      = ImGui::IsKeyDown(ImGuiKey_A);
    const bool right     = ImGui::IsKeyDown(ImGuiKey_D);
    const bool forward   = ImGui::IsKeyDown(ImGuiKey_W);
    const bool backward  = ImGui::IsKeyDown(ImGuiKey_S);
    const bool up        = ImGui::IsKeyDown(ImGuiKey_Space) || ImGui::IsKeyDown(ImGuiKey_E);
    const bool down      = ImGui::IsKeyDown(ImGuiKey_C)     || ImGui::IsKeyDown(ImGuiKey_Q);

    auto camera_up       = glm::vec3(0.0f, 1.0f, 0.0f);
    auto camera_right    = m_camera.rotation * RIGHT;
    auto camera_forward  = m_camera.rotation * FORWARD;

    glm::vec3 final_direction = glm::zero<glm::vec3>();

    if (left ^ right)
    {
      final_direction += camera_right * (right ? 1.0f : -1.0f);
    }

    if (forward ^ backward)
    {
      final_direction += camera_forward * (forward ? 1.0f : -1.0f);
    }

    if (up ^ down)
    {
      final_direction += camera_up * (up ? 1.0f : -1.0f);
    }

    if (glm::length2(final_direction) > 0.0f)
    {
      final_direction = glm::normalize(final_direction);
      m_camera.position += final_direction * seconds * m_interface_settings.camera_settings.speed;
    }
  }

  // update camera rotation
  {
    auto[pos_x, pos_y]           = ImGui::GetMousePos();
    auto[prev_x, prev_y]         = m_previous_mouse_position;
    auto[drag_x, drag_y]         = std::array{pos_x - prev_x, pos_y - prev_y};
    m_previous_mouse_position[0] = pos_x;
    m_previous_mouse_position[1] = pos_y;

    if (ImGui::IsMouseDown(ImGuiMouseButton_Right))
    {
      // update the y-rotation first (rotate around UP axis)
      auto rotation_y = glm::angleAxis(
        glm::radians(-drag_x * m_interface_settings.camera_settings.sensitivity), UP);
      m_camera.rotation = rotation_y * m_camera.rotation;

      // then update x-rotation
      auto rotation_x = glm::angleAxis(
        glm::radians(-drag_y * m_interface_settings.camera_settings.sensitivity),
        m_camera.rotation * RIGHT);
      m_camera.rotation = rotation_x * m_camera.rotation;
    }
  }
}

//============================================================================//
void demo_app::do_rendering() noexcept
{
  // we resize the screen viewport
  auto* surface = SDL_GetWindowSurface(m_window);
  glViewport(0, 0, surface->w, surface->h);

  // render the demo
  this->render_world();

  // render ImGui interface
  this->render_interface();
}

//============================================================================//
void demo_app::init_window_and_opengl() noexcept
{
  // Initialize SDL
  int retval = SDL_Init(SDL_INIT_VIDEO);
  OIT_ASSERT(retval == 0, "[Error] Failed to initialize SDL");

  // Create SDL window
  m_window = SDL_CreateWindow(
    WINDOW_NAME,
    WINDOW_SIZE[0],
    WINDOW_SIZE[1],
    SDL_WINDOW_MAXIMIZED | SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
  OIT_ASSERT(m_window, "[Error] Failed to create SDL window");

  int contentFlags = 0;
  #ifdef OIT_OPENGL_DEBUG
  contentFlags |= SDL_GL_CONTEXT_DEBUG_FLAG; // allows use of opengl debugging
  #endif
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, contentFlags);

  // Ask for a core profile, OpenGL 4.3
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

  // Create OpenGL context
  m_opengl_context = SDL_GL_CreateContext(m_window);
  OIT_ASSERT(m_opengl_context, "[Error] Failed to create OpenGL context");

  // Make the context current for the window we created before
  retval = SDL_GL_MakeCurrent(m_window, m_opengl_context);
  OIT_ASSERT(retval == 0, "[Error] Failed to make to OpenGL context current");

  // GLAD - initialize OpenGL bindings
  retval = gladLoadGLLoader(reinterpret_cast<GLADloadproc>(SDL_GL_GetProcAddress));
  OIT_ASSERT(retval, "[Error] Failed to initialize GLAD");

  // Enable OpenGL debugging
  #ifdef OIT_OPENGL_DEBUG
  glEnable(GL_DEBUG_OUTPUT);
  glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
  glDebugMessageCallback(&opengl_message_callback, nullptr);
  glDebugMessageControl(
    GL_DEBUG_SOURCE_API,
    GL_DEBUG_TYPE_ERROR,
    GL_DEBUG_SEVERITY_HIGH,
    0, nullptr, GL_TRUE);
  #endif

  glGenVertexArrays(1, &m_vao);
  glBindVertexArray(m_vao);

  glEnable(GL_DEPTH_TEST);

  // disable vsync
  SDL_GL_SetSwapInterval(0);

  this->on_screen_resize();
}

//============================================================================//
void demo_app::init_imgui() noexcept
{
  // Setup Dear ImGui context
  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO& io = ImGui::GetIO();
  io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls

  // Setup Platform/Renderer backends
  ImGui_ImplSDL3_InitForOpenGL(m_window, m_opengl_context);
  ImGui_ImplOpenGL3_Init();
}

//============================================================================//
void demo_app::render_interface() noexcept
{
  this->render_interface_top_menu();
  this->render_interface_scene();
  this->render_interface_performance();
  this->render_interface_camera();
}

//============================================================================//
void demo_app::render_interface_top_menu() noexcept
{
  // top menu window
  if (ImGui::BeginMainMenuBar())
  {
    if (ImGui::BeginMenu("Windows"))
    {
      // scene
      ImGui::MenuItem("Scene", nullptr, &m_interface_settings.scene_window_visible);

      // performance
      ImGui::MenuItem("Performance", nullptr, &m_interface_settings.performance_window_visible);

      // camera
      ImGui::MenuItem("Camera", nullptr, &m_interface_settings.camera_window_visible);

      ImGui::EndMenu();
    }

    ImGui::EndMainMenuBar();
  }
}

//============================================================================//
void demo_app::render_interface_scene() noexcept
{
  // scene menu
  if (m_interface_settings.scene_window_visible)
  {
    if (ImGui::Begin("Scene", &m_interface_settings.scene_window_visible))
    {
      int current_item = m_transparency_method;
      if (ImGui::Combo("Transparency rendering method", &current_item, OIT_METHOD_NAMES, oit_methods::count))
      {
        m_transparency_method = static_cast<oit_methods::types>(current_item);
      }

      ImGui::ColorEdit3("background color", &m_interface_settings.background_color.x);
      ImGui::Checkbox("Face culling", &m_interface_settings.do_face_culling);
      ImGui::Separator();

      switch (m_transparency_method)
      {
        using namespace algorithms;
        case oit_methods::solid_only:       solid_only::draw_interface(m_algorithm_data.solid_data);    break;
        case oit_methods::weighted_sum:     meshkin::draw_interface(m_algorithm_data.meshkin_data);     break;
        case oit_methods::depth_peeling:    depth_peeling::draw_interface(m_algorithm_data.dp_data);    break;
        case oit_methods::weighted_average: weighted_average::draw_interface(m_algorithm_data.wa_data); break;
        case oit_methods::wboit:            wboit::draw_interface(m_algorithm_data.wboit_data);         break;
        case oit_methods::moment:           moment::draw_interface(m_algorithm_data.moment_data);       break;
      }

      ImGui::Separator();

      if (ImGui::TreeNode("scene tree"))
      {
        if (ImGui::Button("add"))
        {
          ImGui::OpenPopup("add popup");
        }

        ImGui::SameLine();

        constexpr const char* FILE_PATTERNS[1] = {"*.json"};

        if (ImGui::Button("save to file"))
        {
          cstr path = tinyfd_saveFileDialog(
            "save scene dialog", nullptr, 1, FILE_PATTERNS, nullptr);

          if (path)
          {
            this->serialize_scene_into_file(path);
          }
        }

        ImGui::SameLine();

        if (ImGui::Button("load from file"))
        {
          cstr path = tinyfd_openFileDialog(
            "load scene dialog", nullptr, 1, FILE_PATTERNS, nullptr, 0);

          if (path)
          {
            this->deserialize_scene_from_file(path);
          }
        }

        if (ImGui::BeginPopup("add popup"))
        {
          render_scene scene{};

          if (ImGui::Selectable("load from file"))
          {
            auto* path = tinyfd_openFileDialog(
              "load scene from model file",
              nullptr,
              0,
              nullptr,
              nullptr,
              0);

            if (path)
            {
              scene = geometry::create_render_scene_from_model_file(path);
            }
          }

          if (ImGui::Selectable("small particle system [1]"))
          {
            scene = geometry::create_particle_cloud(1);
          }

          if (ImGui::Selectable("medium particle system [16]"))
          {
            scene = geometry::create_particle_cloud(16);
          }

          if (ImGui::Selectable("big particle system [32]"))
          {
            scene = geometry::create_particle_cloud(32);
          }

          if (scene.objects.size() > 0)
          {
            m_scenes.push_back(std::move(scene));
          }

          ImGui::EndPopup();
        }

        ImGui::Separator();

        int id_counter = 420;

        s32 scene_index_to_erase = -1;

        for (u64 i = 0; i < m_scenes.size(); ++i)
        {
          auto& scene = m_scenes[i];
          ImGui::PushID(id_counter);

          if (ImGui::TreeNode(scene.file_path.c_str()))
          {
            if (ImGui::Button("delete"))
            {
              scene_index_to_erase = static_cast<s32>(i);
            }

            ImGui::NewLine();

            constexpr f32 POS_LIMIT       = 32;
            constexpr f32 SCALE_LIMIT_MIN = 0.001f;
            constexpr f32 SCALE_LIMIT_MAX = 32.0f;

            ImGui::SliderFloat3("position", &scene.position.x, -POS_LIMIT, POS_LIMIT);
            ImGui::SliderFloat("scale", &scene.scale, SCALE_LIMIT_MIN, SCALE_LIMIT_MAX);
            ImGui::ColorEdit4("color override", &scene.color_override.x);
            ImGui::TreePop();
          }

          ImGui::PopID();

          id_counter += 3;
        }

        // we need to erase stuff AFTER the imgui tree loop ends
        if (scene_index_to_erase >= 0)
        {
          OIT_ASSERT(scene_index_to_erase < m_scenes.size());
          OIT_ASSERT(m_scenes.size());

          // swap with the last object
          if (scene_index_to_erase != m_scenes.size()-1)
          {
            std::swap(m_scenes[scene_index_to_erase], m_scenes[m_scenes.size() - 1]);
          }

          // deallocate the last object (the scene we want to delete)
          for (auto& render_object : m_scenes.back().objects)
          {
            deallocate_render_object(render_object);
          }

          // and pop it
          m_scenes.pop_back();
        }

        ImGui::TreePop();
      }
    }
    ImGui::End();
  }
}

//============================================================================//
void demo_app::render_interface_performance() noexcept
{
  // performance
  if (m_interface_settings.performance_window_visible)
  {
    if (ImGui::Begin("Performance", &m_interface_settings.performance_window_visible))
    {
      auto& perf  = m_interface_settings.performance;

      int ms = static_cast<int>(perf.frametime * 1000.0f);

      perf.frametime_sum += perf.frametime;
      perf.sum_members   += 1;

      if (perf.frametime_sum > perf.NUM_SECONDS_AVG)
      {
        perf.last_fps = static_cast<int>(1.0f / (perf.frametime_sum / perf.sum_members));
        perf.frametime_sum = 0.0f;
        perf.sum_members   = 0;
      }

      ImGui::Text("Frametime: %dms", ms);
      ImGui::Text("FPS: %d", perf.last_fps);
    }
    ImGui::End();
  }
}

//============================================================================//
void demo_app::render_interface_camera() noexcept
{
  // camera settings
  if (m_interface_settings.camera_window_visible)
  {
    if (ImGui::Begin("Camera Settings", &m_interface_settings.camera_window_visible))
    {
      ImGui::SliderFloat("Camera Speed",       &m_interface_settings.camera_settings.speed, 0.1f, 25.0f);
      ImGui::SliderFloat("Camera Sensitivity", &m_interface_settings.camera_settings.sensitivity, 0.1f, 25.0f);

      if (ImGui::SliderFloat("Camera FOV", &m_camera.fov_horizontal_degrees, 10.0f, 120.0f))
      {
        this->on_screen_resize();
      }
    }
    ImGui::End();
  }
}

//============================================================================//
bool demo_app::serialize_scene_into_file(cstr file_name) noexcept
{
  nlohmann::json json_data;

  json_data["back"] = std::array
  {
    m_interface_settings.background_color.r,
    m_interface_settings.background_color.g,
    m_interface_settings.background_color.b,
  };

  auto& objects = json_data["objects"];
  for (auto& scene : m_scenes)
  {
    nlohmann::json ser;

    this->serialize_scene(ser, scene, false);

    objects.push_back(std::move(ser));
  }

  std::ofstream file(file_name, std::ios_base::out);
  if (!file.is_open())
  {
    return false;
  }

  file << json_data;

  return true;
}

//============================================================================//
bool demo_app::deserialize_scene_from_file(cstr file_name) noexcept
{
  std::fstream file(file_name, std::ios_base::in);
  if (!file.is_open())
  {
    log("file not found");
    return false;
  }

  // clean after ourselves
  this->cleanup_scene();

  try
  {
    nlohmann::json json_data = nlohmann::json::parse(file);

    // background color
    auto& back = json_data["back"];
    for (int i = 0; i < 3; ++i)
    {
      float& c = *(&m_interface_settings.background_color.x + i);
      c = back[i].get<float>();
    }

    // objects
    auto& objects = json_data["objects"];
    for (auto& object : objects)
    {
      render_scene scene;

      if (!this->serialize_scene(object, scene, true))
      {
        return false;
      }

      m_scenes.push_back(std::move(scene));
    }
  }
  catch ([[maybe_unused]]std::exception& ex)
  {
    return false;
  }

  return true;
}

//============================================================================//
bool demo_app::serialize_scene(nlohmann::json& json, render_scene& scene, bool deserialize) noexcept
{
  if (deserialize)
  {
    try
    {
      auto& p = json["position"];
      auto& r = json["rotation"];
      auto& s = json["scale"];
      auto& c = json["color_override"];
      auto& t = json["type"];

      // position
      glm::vec3 position =
      {
        p[0].get<float>(),
        p[1].get<float>(),
        p[2].get<float>()
      };

      // rotation
      glm::quat rotation = glm::quat(glm::radians(glm::vec3
      {
        r[0].get<float>(),
        r[1].get<float>(),
        r[2].get<float>()
      }));

      // scale
      float scale = s.get<float>();

      // color override
      glm::vec4 color_override =
      {
        c[0].get<float>(),
        c[1].get<float>(),
        c[2].get<float>(),
        c[3].get<float>(),
      };

      if (t.get<std::string>() == "mesh")
      {
        auto path = json["file_path"].get<std::string>();
        scene = geometry::create_render_scene_from_model_file(path.c_str());
      }
      else  // particle
      {
        int count = json["count"].get<int>();
        scene = geometry::create_particle_cloud(count);
      }

      scene.position       = position;
      scene.rotation       = rotation;
      scene.scale          = scale;
      scene.color_override = color_override;

      return true;
    }
    catch ([[maybe_unused]]std::exception& ex)
    {
      return false;
    }
  }
  else
  {
    json["position"] = std::array
    {
      scene.position.x,
      scene.position.y,
      scene.position.z
    };

    glm::vec3 degrees = glm::degrees(glm::eulerAngles(scene.rotation));
    json["rotation"] = std::array
    {
      degrees.x,
      degrees.y,
      degrees.z,
    };

    json["scale"] = scene.scale;

    json["color_override"] = std::array
    {
      scene.color_override.x,
      scene.color_override.y,
      scene.color_override.z,
      scene.color_override.w,
    };

    if (scene.type == scene_type::loaded_mesh)
    {
      json["type"]      = "mesh";
      json["file_path"] = scene.file_path;
    }
    else
    {
      json["type"]  = "particle";
      json["count"] = scene.objects.size();
    }

    return true;
  }
}

//============================================================================//
void demo_app::render_world() noexcept
{
  using namespace oit_methods;
  using namespace oit::algorithms;

  if (m_interface_settings.do_face_culling)
  {
    glEnable(GL_CULL_FACE);
  }
  else
  {
    glDisable(GL_CULL_FACE);
  }

  common::render_data frame_data
  {
    .camera            = m_camera,
    .projection_matrix = m_camera_projection_cached,
    .background_color  = m_interface_settings.background_color,
  };

  switch (m_transparency_method)
  {
    case oit_methods::solid_only:       solid_only::render_world(m_algorithm_data.solid_data, frame_data, m_scenes);    break;
    case oit_methods::depth_peeling:    depth_peeling::render_world(m_algorithm_data.dp_data, frame_data, m_scenes);    break;
    case oit_methods::weighted_sum:     meshkin::render_world(m_algorithm_data.meshkin_data, frame_data, m_scenes);     break;
    case oit_methods::weighted_average: weighted_average::render_world(m_algorithm_data.wa_data, frame_data, m_scenes); break;
    case oit_methods::wboit:            wboit::render_world(m_algorithm_data.wboit_data, frame_data, m_scenes);         break;
    case oit_methods::moment:           moment::render_world(m_algorithm_data.moment_data, frame_data, m_scenes);       break;
  }
}

//============================================================================//
void demo_app::on_screen_resize() noexcept
{
  using namespace oit::algorithms;

  int width, height;
  SDL_GetWindowSize(m_window, &width, &height);

  // recalculate camera projection matrix
  float aspect = static_cast<float>(width) / height;
  m_camera_projection_cached = calculate_camera_projection(m_camera, aspect);

  // regenerate framebuffers for oit methods that require so
  meshkin::on_resize_screen(
    m_algorithm_data.meshkin_data, width, height);
  depth_peeling::on_resize_screen(
    m_algorithm_data.dp_data, width, height);
  weighted_average::on_resize_screen(
    m_algorithm_data.wa_data, width, height);
  wboit::on_resize_screen(
    m_algorithm_data.wboit_data, width, height);
  moment::on_resize_screen(
    m_algorithm_data.moment_data, width, height);
}

//============================================================================//
void demo_app::cleanup_scene() noexcept
{
  // cleanup scene
  for (auto& scene : m_scenes)
  {
    for (auto& render_object : scene.objects)
    {
      deallocate_render_object(render_object);
    }
  }

  m_scenes.clear();
}

//============================================================================//
#pragma optimize("", off)   // For some reason (in MSVC) the following function
                            // is optimized in release builds in a way that
                            // everything stops working..
                            // No idea why, but we have to turn off optimization
                            // for this piece of code
void demo_app::init_oit_algorithms() noexcept
{
  using namespace oit::algorithms;

  int width, height;
  SDL_GetWindowSize(m_window, &width, &height);

  m_algorithm_data.solid_data   = solid_only::init();
  m_algorithm_data.dp_data      = depth_peeling::init(width, height);
  m_algorithm_data.meshkin_data = meshkin::init(width, height);
  m_algorithm_data.wa_data      = weighted_average::init(width, height);
  m_algorithm_data.wboit_data   = wboit::init(width, height);
  m_algorithm_data.moment_data  = moment::init(width, height);
}
#pragma optimize("", on)

}
