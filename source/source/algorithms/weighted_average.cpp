//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#include <algorithms/weighted_average.h>

#include <shaders.h>

#include <IMGUI/imgui.h>

namespace oit::algorithms::weighted_average {

//============================================================================//
temp_data init(int width, int height) noexcept
{
  temp_data data;
  on_resize_screen(data, width, height);

  data.viewport_width  = width;
  data.viewport_height = height;

  // solid
  data.solid_gpu_program
    = oit::compile_and_link_gpu_program(
      temp_data::SOLID_SHADERS[0],
      temp_data::SOLID_SHADERS[1]);

  // accum
  data.accumulate_gpu_program
    = oit::compile_and_link_gpu_program(
      temp_data::ACCUM_SHADERS[0],
      temp_data::ACCUM_SHADERS[1]);

  // oit
  data.oit_gpu_program
    = oit::compile_and_link_gpu_program(
      temp_data::OIT_SHADERS[0],
      temp_data::OIT_SHADERS[1]);

  return data;
}

//============================================================================//
void on_resize_screen(temp_data& temp, int width, int height) noexcept
{
  common::delete_framebuffer(temp.f1);
  common::delete_accum(temp.f2);

  auto single_precision = temp.f16_precision ? GL_R16F    : GL_R32F;
  auto quad_precision   = temp.f16_precision ? GL_RGBA16F : GL_RGBA32F;

  common::generate_framebuffer(temp.f1, width, height);
  common::generate_accum(temp.f2, width, height, single_precision, quad_precision);

  temp.viewport_width  = width;
  temp.viewport_height = height;
}

//============================================================================//
void draw_interface(temp_data& temp) noexcept
{
  if (ImGui::Checkbox("f16 precision", &temp.f16_precision))
  {
    on_resize_screen(temp, temp.viewport_width, temp.viewport_height);
  }
}

//============================================================================//
void terminate(temp_data& temp) noexcept
{
  common::delete_framebuffer(temp.f1);
  common::delete_accum(temp.f2);

  if (temp.oit_gpu_program)
  {
    glDeleteProgram(temp.oit_gpu_program);
    temp.oit_gpu_program = 0;
  }

  if (temp.accumulate_gpu_program)
  {
    glDeleteProgram(temp.accumulate_gpu_program);
    temp.accumulate_gpu_program = 0;
  }

  if (temp.solid_gpu_program)
  {
    glDeleteProgram(temp.solid_gpu_program);
    temp.solid_gpu_program = 0;
  }
}

//============================================================================//
void render_world(
  temp_data&                 temp,
  const common::render_data& data,
  std::span<render_scene>    scenes) noexcept
{
  // render solid stuff into F1 framebuffer
  {
    glBindFramebuffer(GL_FRAMEBUFFER, temp.f1.frame_buffer_handle);
    common::clear_color_and_depth(data.background_color);
    glUseProgram(temp.solid_gpu_program);
    common::render_scene(data, scenes, object_flags::opaque);
  }

  // blit depth to F2
  {
    glBindFramebuffer(GL_READ_FRAMEBUFFER, temp.f1.frame_buffer_handle);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, temp.f2.frame_buffer_handle);
    glBlitFramebuffer(
      0, 0, temp.viewport_width, temp.viewport_height,
      0, 0, temp.viewport_width, temp.viewport_height,
      GL_DEPTH_BUFFER_BIT, GL_NEAREST);
  }

  // render scene to F2 and accumulate data
  {
    glUseProgram(temp.accumulate_gpu_program);
    glBindFramebuffer(GL_FRAMEBUFFER, temp.f2.frame_buffer_handle);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glActiveTexture(GL_TEXTURE0);
    glUniform1i(42, 0);
    GLenum buffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
    glDrawBuffers(2, buffers);

    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE);
    common::render_scene(data, scenes, object_flags::transparent | object_flags::empty);
    glDepthMask(GL_TRUE);
  }

  // render the transparency to F1
  {
    glBindFramebuffer(GL_FRAMEBUFFER, temp.f1.frame_buffer_handle);
    glUseProgram(temp.oit_gpu_program);

    // accumulator texture
    glActiveTexture(GL_TEXTURE0 + 2);
    glBindTexture(GL_TEXTURE_2D, temp.f2.quad_component_handle);
    glUniform1i(42, 2);
    // counter texture
    glActiveTexture(GL_TEXTURE0 + 1);
    glBindTexture(GL_TEXTURE_2D, temp.f2.single_component_handle);
    glUniform1i(43, 1);

    // draw fullscreen quad
    glDisable(GL_DEPTH_TEST);
    glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_SRC_ALPHA);

    glDrawArrays(GL_TRIANGLES, 0, 3);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);

    glDisable(GL_BLEND);

    glActiveTexture(GL_TEXTURE0);
  }

  // blit to back
  {
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, temp.f1.frame_buffer_handle);
    glReadBuffer(GL_COLOR_ATTACHMENT0);
    glBlitFramebuffer(
      0, 0, temp.viewport_width, temp.viewport_height,
      0, 0, temp.viewport_width, temp.viewport_height,
      GL_COLOR_BUFFER_BIT, GL_NEAREST);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }
}

}

