//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#include <algorithms/meshkin.h>

#include <camera.h>
#include <shaders.h>

#include <IMGUI/imgui.h>

namespace oit::algorithms::meshkin {

//============================================================================//
static void delete_resources(temp_data& data)
{
  common::delete_framebuffer(data.f1);
  common::delete_framebuffer(data.f2);
}

//============================================================================//
static void regenerete_framebuffer(temp_data& data, int width, int height)
{
  // cleanup if required
  delete_resources(data);
  
  common::generate_framebuffer(data.f1, width, height);
  common::generate_framebuffer(data.f2, width, height, true, GL_RGBA16F);

  data.viewport_width  = width;
  data.viewport_height = height;
}

//============================================================================//
temp_data init(int width, int height) noexcept
{
  temp_data data{};
  regenerete_framebuffer(data, width, height);

  data.solid_gpu_program = oit::compile_and_link_gpu_program(
    SOLID_SHADERS[0],
    SOLID_SHADERS[1]);

  data.accum_gpu_program = oit::compile_and_link_gpu_program(
    ACCUM_SHADERS[0],
    ACCUM_SHADERS[1]);

  data.oit_gpu_program = oit::compile_and_link_gpu_program(
    OIT_SHADERS[0],
    OIT_SHADERS[1]);

  return data;
}

//============================================================================//
void on_resize_screen(temp_data& temp, int width, int height) noexcept
{
  regenerete_framebuffer(temp, width, height);
}

//============================================================================//
void draw_interface(temp_data& temp) noexcept
{
  ImGui::Checkbox("debug", &temp.draw_debug);
}

//============================================================================//
void terminate(temp_data& temp) noexcept
{
  delete_resources(temp);
}

//============================================================================//
void render_world(
  temp_data&                 temp,
  const common::render_data& data,
  std::span<render_scene>    scenes) noexcept
{
  using namespace oit;

  // render opaque surfaces to F0
  {
    glBindFramebuffer(GL_FRAMEBUFFER, temp.f1.frame_buffer_handle);
    common::clear_color_and_depth(data.background_color);

    glUseProgram(temp.solid_gpu_program);
    glActiveTexture(GL_TEXTURE0);

    // render solid background it here...
    common::render_scene(data, scenes, object_flags::opaque);
  }

  // accumulate transparent surfaces to F2
  {
    glBindFramebuffer(GL_FRAMEBUFFER, temp.f2.frame_buffer_handle);
    common::clear_color_and_depth(glm::vec4(0.0f));

    // blit depth from F1 into F2
    common::blit(
      temp.f1.frame_buffer_handle, temp.f2.frame_buffer_handle,
      GL_DEPTH_BUFFER_BIT, temp.viewport_width, temp.viewport_height);

    glBindFramebuffer(GL_FRAMEBUFFER, temp.f2.frame_buffer_handle);

    glUseProgram(temp.accum_gpu_program);

    glDepthMask(GL_FALSE);        // do not write into depth buffer
    glEnable(GL_BLEND);           // enable blending
    glBlendFunc(GL_ONE, GL_ONE);  // tweak blend function

    // render transparent parts into accumulator
    glActiveTexture(GL_TEXTURE0);
    glUniform1i(uniforms::object_texture, 0); // object texture

    common::render_scene(data, scenes, object_flags::transparent | object_flags::empty);

    // reset GPU pipeline to initial state
    glDepthMask(GL_TRUE);
    glBlendFunc(GL_ONE_MINUS_SRC_COLOR, GL_SRC_COLOR);
    glDisable(GL_BLEND);
  }

  // full screen quad for final composition
  {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glUseProgram(temp.oit_gpu_program);

    // accumulator
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, temp.f2.color_component_handle);
    glUniform1i(uniforms::accumulator_texture, 0);

    // background
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, temp.f1.color_component_handle);
    glUniform1i(uniforms::background_texture, 1);

    glDisable(GL_DEPTH_TEST);           // disable depth testing
    glDrawArrays(GL_TRIANGLES, 0, 3);   // full screen pass
    glEnable(GL_DEPTH_TEST);
    glActiveTexture(GL_TEXTURE0);
  }
}

}

