//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#include <algorithms/wboit.h>

#include <shaders.h>
#include <camera.h>

#include <IMGUI/imgui.h>

namespace oit::algorithms::wboit {

//============================================================================//
temp_data init(int width, int height) noexcept
{
  temp_data data;

  on_resize_screen(data, width, height);

  // solid
  data.solid_gpu_program
    = oit::compile_and_link_gpu_program(
      SOLID_SHADERS[0],
      SOLID_SHADERS[1]);

  // accum
  data.accumulate_gpu_program
    = oit::compile_and_link_gpu_program(
      ACCUM_SHADERS[0],
      ACCUM_SHADERS[1]);

  // oit
  data.oit_gpu_program
    = oit::compile_and_link_gpu_program(
      OIT_SHADERS[0],
      OIT_SHADERS[1]);

  return data;
}

//============================================================================//
void on_resize_screen(temp_data& temp, int width, int height) noexcept
{
  common::delete_framebuffer(temp.f1);
  common::delete_accum(temp.f2);

  auto single_format = temp.accuracy == accum_accuracy::F32
    ? GL_R32F
    : GL_R16F;

  auto quad_format = temp.accuracy == accum_accuracy::F32
    ? GL_RGBA32F
    : GL_RGBA16F;

  common::generate_framebuffer(temp.f1,  width, height, false);
  common::generate_accum(temp.f2, width, height, single_format, quad_format);

  temp.viewport_width  = width;
  temp.viewport_height = height;
}

//============================================================================//
void draw_interface(temp_data& temp) noexcept
{
  // weight functions
  if (int current = temp.weights;
    ImGui::Combo(
      "Weight function", &current, WEIGHT_FUNCTIONS_STR, weight_function::count))
  {
    temp.weights = static_cast<weight_function::etype>(current);
  }

  // accumulator accuracy
  if (int current = temp.accuracy;
    ImGui::Combo(
      "Accumulator accuracy", &current, ACCUM_ACCURACY_STR, accum_accuracy::count))
  {
    temp.accuracy = static_cast<accum_accuracy::etype>(current);
    on_resize_screen(temp, temp.viewport_width, temp.viewport_height);
  }

  ImGui::SliderFloat("min dist", &temp.scale_near, 0.0f, 1'000.0f);
  ImGui::SliderFloat("max dist", &temp.scale_far,  0.0f, 1'000.0f);

  temp.scale_near = std::min(temp.scale_near, temp.scale_far);
  temp.scale_far  = std::max(temp.scale_near, temp.scale_far);
}

//============================================================================//
void terminate(temp_data& temp) noexcept
{
  common::delete_framebuffer(temp.f1);
  common::delete_accum(temp.f2);

  if (temp.solid_gpu_program)
  {
    glDeleteProgram(temp.solid_gpu_program);
    temp.solid_gpu_program = 0;
  }

  if (temp.oit_gpu_program)
  {
    glDeleteProgram(temp.oit_gpu_program);
    temp.oit_gpu_program = 0;
  }

  if (temp.accumulate_gpu_program)
  {
    glDeleteProgram(temp.accumulate_gpu_program);
    temp.accumulate_gpu_program = 0;
  }
}

//============================================================================//
void render_world(
  temp_data&                 temp,
  const common::render_data& data,
  std::span<render_scene>    scenes) noexcept
{
  auto& f1 = temp.f1;

  // render solid stuff into F1 framebuffer
  {
    glBindFramebuffer(GL_FRAMEBUFFER, f1.frame_buffer_handle);
    common::clear_color_and_depth(data.background_color);
    glUseProgram(temp.solid_gpu_program);
    common::render_scene(data, scenes, object_flags::opaque);
  }

  // blit depth to F2
  {
    common::blit(
      f1.frame_buffer_handle,
      temp.f2.frame_buffer_handle,
      GL_DEPTH_BUFFER_BIT,
      temp.viewport_width,
      temp.viewport_height);
  }

  // render scene to F2 and accumulate data
  {
    constexpr float one[4]  = {1.0f, 1.0f, 1.0f, 1.0f};
    constexpr float zero[4] = {0.0f, 0.0f, 0.0f, 0.0f};

    glUseProgram(temp.accumulate_gpu_program);
    glBindFramebuffer(GL_FRAMEBUFFER, temp.f2.frame_buffer_handle);
    GLenum buffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
    glDrawBuffers(2, buffers);

    glClearBufferfv(GL_COLOR, 0, one);
    glClearBufferfv(GL_COLOR, 1, zero);

    glActiveTexture(GL_TEXTURE1);
    glUniform1i(uniforms::depth_sampler, 1);
    glBindTexture(GL_TEXTURE_2D, f1.depth_component_handle);

    glActiveTexture(GL_TEXTURE0);
    glUniform1i(common::uniforms::texture_sampler, 0);

    glUniform4i(uniforms::weight_method, temp.weights, 0, 0, 0);
    glUniform4f(uniforms::near_far, data.camera.near, data.camera.far, 0.0f, 0.0f);
    glUniform4f(uniforms::scale_near_far, temp.scale_near, temp.scale_far, 0.0f, 0.0f);

    glEnable(GL_BLEND);

    glDepthMask(GL_FALSE);
    glBlendFunci(0, GL_ZERO, GL_ONE_MINUS_SRC_ALPHA);      // coverage
    glBlendFunci(1, GL_ONE, GL_ONE);                       // accumulator

    common::render_scene(data, scenes, object_flags::transparent | object_flags::empty);

    glBlendFunci(1, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // accumulator
    glBlendFunci(0, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // coverage
    glDepthMask(GL_TRUE);
  }

  // render the transparency to F1
  {
    glBindFramebuffer(GL_FRAMEBUFFER, f1.frame_buffer_handle);
    glUseProgram(temp.oit_gpu_program);

    // accumulator texture
    glActiveTexture(GL_TEXTURE0 + 2);
    glBindTexture(GL_TEXTURE_2D, temp.f2.quad_component_handle);
    glUniform1i(common::uniforms::texture_sampler, 2);
    // counter texture
    glActiveTexture(GL_TEXTURE0 + 1);
    glBindTexture(GL_TEXTURE_2D, temp.f2.single_component_handle);
    glUniform1i(43, 1);

    glActiveTexture(GL_TEXTURE0);

    glDisable(GL_DEPTH_TEST);
    glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_SRC_ALPHA);

    // draw fullscreen quad
    glDrawArrays(GL_TRIANGLES, 0, 3);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);

    glDisable(GL_BLEND);
  }

  // blit to back
  {
    common::blit(
      f1.frame_buffer_handle, 0, GL_COLOR_BUFFER_BIT,
      temp.viewport_width, temp.viewport_height);
  }
}

}

