//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#include <algorithms/moment.h>

#include <shaders.h>
#include <camera.h>

#include <IMGUI/imgui.h>

namespace oit::algorithms::moment {

//============================================================================//
static void delete_framebuffers(temp_data& data)
{
  common::delete_framebuffer(data.f1);
  common::delete_accum(data.f2);
  common::delete_accum(data.f3);
}

//============================================================================//
temp_data init(int width, int height) noexcept
{
  temp_data data;

  on_resize_screen(data, width, height);
  
  data.solid_gpu_program = oit::compile_and_link_gpu_program(
    SOLID_SHADERS[0], SOLID_SHADERS[1]);

  data.moment_gpu_program = oit::compile_and_link_gpu_program(
    MOMENT_SHADERS[0], MOMENT_SHADERS[1]);

  data.accumulate_gpu_program = oit::compile_and_link_gpu_program(
    ACCUM_SHADERS[0], ACCUM_SHADERS[1]);

  data.oit_gpu_program = oit::compile_and_link_gpu_program(
    OIT_SHADERS[0], OIT_SHADERS[1]);

  return data;
}

//============================================================================//
void on_resize_screen(temp_data& temp, int width, int height) noexcept
{
  temp.viewport_width  = width;
  temp.viewport_height = height;

  delete_framebuffers(temp);

  auto single_precision = temp.f16_accumulators ? GL_R16F    : GL_R32F;
  auto quad_precision   = temp.f16_accumulators ? GL_RGBA16F : GL_RGBA32F;

  common::generate_framebuffer(temp.f1, width, height);
  common::generate_accum(temp.f2, width, height, single_precision, quad_precision);
  common::generate_accum(temp.f3, width, height, single_precision, quad_precision);
}

//============================================================================//
void draw_interface(temp_data& temp) noexcept
{
  if (ImGui::Checkbox("f16 precision", &temp.f16_accumulators))
  {
    on_resize_screen(temp, temp.viewport_width, temp.viewport_height);
  }
}

//============================================================================//
void terminate(temp_data& temp) noexcept
{
  delete_framebuffers(temp);
}

//============================================================================//
void render_world(
  temp_data&                 temp,
  const common::render_data& data,
  std::span<render_scene>    scenes) noexcept
{
  using namespace oit;

  // render solid stuff into F1
  {
    glBindFramebuffer(GL_FRAMEBUFFER, temp.f1.frame_buffer_handle);
    common::clear_color_and_depth(data.background_color);

    glUseProgram(temp.solid_gpu_program);
    glActiveTexture(GL_TEXTURE0);
    common::render_scene(data, scenes, object_flags::opaque);
  }

  // blit depth from F1 to F2 and F3
  {
    common::blit(
      temp.f1.frame_buffer_handle,
      temp.f2.frame_buffer_handle,
      GL_DEPTH_BUFFER_BIT,
      temp.viewport_width,
      temp.viewport_height);

    common::blit(
      temp.f1.frame_buffer_handle,
      temp.f3.frame_buffer_handle,
      GL_DEPTH_BUFFER_BIT,
      temp.viewport_width,
      temp.viewport_height);
  }

  // render transparent objects and calculate moments
  {
    // bind framebuffer
    {
      glBindFramebuffer(GL_FRAMEBUFFER, temp.f2.frame_buffer_handle);
      GLenum buffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1}; // moments sum, moments
      glDrawBuffers(2, buffers);
    }

    // clear colors
    {
      glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
      glClear(GL_COLOR_BUFFER_BIT);
    }

    glUseProgram(temp.moment_gpu_program);

    glActiveTexture(GL_TEXTURE0);
    glUniform1i(common::uniforms::texture_sampler, 0);
    glUniform4f(uniforms::near_far, data.camera.near, data.camera.far, 0.0f, 0.0f);

    // disable depth test
    glDepthMask(GL_FALSE);

    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE);

    common::render_scene(data, scenes, object_flags::transparent | object_flags::empty);
  }

  // render transparent objects again, calculate coverage from moments
  // and accumulate it into buffers
  {
    constexpr float zero[4] = {0.0f, 0.0f, 0.0f, 0.0f};
    constexpr float one [4] = {1.0f, 1.0f, 1.0f, 1.0f};

    glBindFramebuffer(GL_FRAMEBUFFER, temp.f3.frame_buffer_handle);
    GLenum buffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};    // coverage, accumulate
    glDrawBuffers(2, buffers);

    glClearBufferfv(GL_COLOR, 0, one);
    glClearBufferfv(GL_COLOR, 1, zero);

    glUseProgram(temp.accumulate_gpu_program);

    // sum of moments
    {
      glActiveTexture(GL_TEXTURE1);
      glBindTexture(GL_TEXTURE_2D, temp.f2.single_component_handle);
      glUniform1i(uniforms::moments_sum_sampler, 1);
    }

    // moments
    {
      glActiveTexture(GL_TEXTURE2);
      glBindTexture(GL_TEXTURE_2D, temp.f2.quad_component_handle);
      glUniform1i(uniforms::moments_sampler, 2);
    }

    glActiveTexture(GL_TEXTURE0);
    glUniform1i(common::uniforms::texture_sampler, 0);

    glUniform4f(uniforms::near_far, data.camera.near, data.camera.far, 0.0f, 0.0f);

    glBlendFunci(0, GL_ZERO, GL_ONE_MINUS_SRC_ALPHA);      // coverage
    glBlendFunci(1, GL_ONE, GL_ONE);                       // accumulator

    common::render_scene(data, scenes, object_flags::transparent | object_flags::empty);

    glBlendFunci(1, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // accumulator
    glBlendFunci(0, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // coverage
  }

  // full screen oit pass, collect accumulated data
  {
    glBindFramebuffer(GL_FRAMEBUFFER, temp.f1.frame_buffer_handle);
    glUseProgram(temp.oit_gpu_program);

    // accumulator texture
    {
      glActiveTexture(GL_TEXTURE0 + 2);
      glBindTexture(GL_TEXTURE_2D, temp.f3.quad_component_handle);
      glUniform1i(42, 2);
    }

    // counter texture
    {
      glActiveTexture(GL_TEXTURE0 + 1);
      glBindTexture(GL_TEXTURE_2D, temp.f3.single_component_handle);
      glUniform1i(43, 1);
    }

    // object texture (reset)
    glActiveTexture(GL_TEXTURE0);

    glDisable(GL_DEPTH_TEST);
    glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_SRC_ALPHA);

    // draw fullscreen quad
    glDrawArrays(GL_TRIANGLES, 0, 3);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glDisable(GL_BLEND);
  }

  // blit into default framebuffer
  {
    common::blit(
      temp.f1.frame_buffer_handle, 0, GL_COLOR_BUFFER_BIT,
      temp.viewport_width, temp.viewport_height);
  }
}

}

