//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#include <algorithms/solid_only.h>

#include <shaders.h>
#include <camera.h>
#include <geometry.h>

#include <IMGUI/imgui.h>

#include <tuple>
#include <array>

namespace oit::algorithms::solid_only {

//============================================================================//
temp_data init() noexcept
{
  temp_data data;
  data.gpu_program = oit::compile_and_link_gpu_program(
    temp_data::SHADERS[0],
    temp_data::SHADERS[1]);
  return data;
}

//============================================================================//
void terminate(temp_data& temp) noexcept
{
  glDeleteProgram(temp.gpu_program);
}

//============================================================================//
void render_world(
  temp_data&                 temp,
  const common::render_data& data,
  std::span<render_scene>    scenes) noexcept
{
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  common::clear_color_and_depth(data.background_color);
  glUseProgram(temp.gpu_program);

  if (temp.hardware_blend)
  {
    // render solid stuff
    common::render_scene(data, scenes);

    //render transparent stuff with blending
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    common::render_scene(
      data, scenes, object_flags::transparent | object_flags::empty);
    glDisable(GL_BLEND);
    glDepthMask(GL_TRUE);
  }
  else
  {
    common::render_scene(data, scenes, temp.render_flags);
  }
}

//============================================================================//
void draw_interface(temp_data& temp) noexcept
{
  ImGui::Text("Render Flags");
  constexpr auto FLAGS = std::array
  {
    std::make_tuple(object_flags::opaque,      "opaque"),
    std::make_tuple(object_flags::transparent, "transparent"),
    std::make_tuple(object_flags::empty,       "empty"),
  };

  for (auto[flag, name] : FLAGS)
  {
    if (bool op = temp.render_flags & flag;
      ImGui::Checkbox(name, &op))
    {
      temp.render_flags &= ~flag;
      temp.render_flags |= op ? flag : 0;
    }
  }

  ImGui::Checkbox("hardware blending", &temp.hardware_blend);
}

}

