//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//

#include <algorithms/depth_peeling.h>
#include <shaders.h>

#include <IMGUI/imgui.h>

namespace oit::algorithms::depth_peeling {

//============================================================================//
static void delete_framebuffers(temp_data& data)
{
  common::delete_framebuffer(data.f1);
  common::delete_framebuffer(data.f2);
  common::delete_framebuffer(data.f3);
}

//============================================================================//
temp_data init(int width, int height) noexcept
{
  temp_data data{};

  on_resize_screen(data, width, height);

  data.solid_gpu_program = compile_and_link_gpu_program(
    SOLID_SHADERS[0], SOLID_SHADERS[1]);

  data.peel_gpu_program = compile_and_link_gpu_program(
    PEEL_SHADERS[0], PEEL_SHADERS[1]);

  data.merge_gpu_program = compile_and_link_gpu_program(
    MERGE_SHADERS[0], MERGE_SHADERS[1]);

  return data;
}

//============================================================================//
void draw_interface(temp_data& temp) noexcept
{
  if (int it = temp.num_passes; ImGui::SliderInt("Iterations count", &it, 2, 64))
  {
    temp.num_passes = static_cast<u32>(it);
  }
}

//============================================================================//
void terminate(temp_data& temp) noexcept
{
  delete_framebuffers(temp);

  if (temp.solid_gpu_program != 0)
  {
    glDeleteProgram(temp.solid_gpu_program);
    temp.solid_gpu_program = 0;
  }

  if (temp.peel_gpu_program != 0)
  {
    glDeleteProgram(temp.peel_gpu_program);
    temp.peel_gpu_program = 0;
  }

  if (temp.merge_gpu_program != 0)
  {
    glDeleteProgram(temp.merge_gpu_program);
    temp.merge_gpu_program = 0;
  }
}

//============================================================================//
void render_world(temp_data& temp, const common::render_data& data, std::span<render_scene> scenes) noexcept
{
  // cleanup f3 accum
  {
    glBindFramebuffer(GL_FRAMEBUFFER, temp.f3.frame_buffer_handle);
    common::clear_color_and_depth(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
  }

  // initialize depth of f3 as 0.0f
  {
    glDepthMask(GL_TRUE);
    glBindFramebuffer(GL_FRAMEBUFFER, temp.f3.frame_buffer_handle);
    glClearDepth(0.0f);
    glClear(GL_DEPTH_BUFFER_BIT);
    glClearDepth(1.0);
  }

  // render solid stuff into f1
  {
    glUseProgram(temp.solid_gpu_program);

    glBindFramebuffer(GL_FRAMEBUFFER, temp.f1.frame_buffer_handle);
    common::clear_color_and_depth(data.background_color);

    glActiveTexture(GL_TEXTURE0);

    common::render_scene(data, scenes, object_flags::opaque);
  }

  // repeat
  for (u32 i = 0; i < temp.num_passes; ++i)
  {
    // blit solid depth to f2 from f1
    {
      common::blit(
        temp.f1.frame_buffer_handle,
        temp.f2.frame_buffer_handle,
        GL_DEPTH_BUFFER_BIT,
        temp.viewport_width,
        temp.viewport_height);
    }

    // render only the closest transparent layer into f2
    {
      glBindFramebuffer(GL_FRAMEBUFFER, temp.f2.frame_buffer_handle);
      glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
      glClear(GL_COLOR_BUFFER_BIT);

      // use the peeling program
      glUseProgram(temp.peel_gpu_program);

      glActiveTexture(GL_TEXTURE1);
      glUniform1i(uniforms::additional_depth, 1);
      glBindTexture(GL_TEXTURE_2D, temp.f3.depth_component_handle);
      
      glActiveTexture(GL_TEXTURE0);
      glUniform1i(common::uniforms::texture_sampler, 0);

      common::render_scene(data, scenes, object_flags::transparent | object_flags::empty);
    }

    // blend the closest transparent layer into f3
    {
      glBindFramebuffer(GL_FRAMEBUFFER, temp.f3.frame_buffer_handle);

      glUseProgram(temp.merge_gpu_program);
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, temp.f2.color_component_handle);

      glDisable(GL_DEPTH_TEST);
      glEnable(GL_BLEND);
      glBlendFuncSeparate(GL_DST_ALPHA, GL_ONE, GL_ZERO, GL_ONE_MINUS_SRC_ALPHA);

      // draw fullscreen quad
      glDrawArrays(GL_TRIANGLES, 0, 3);

      glBlendFuncSeparate(GL_ONE, GL_ZERO, GL_ONE, GL_ZERO);
      glDisable(GL_BLEND);
      glEnable(GL_DEPTH_TEST);
    }

    // copy depth from f2 into f3 and use it next round
    if (i != temp.num_passes - 1)
    {
      common::blit(
        temp.f2.frame_buffer_handle,
        temp.f3.frame_buffer_handle,
        GL_DEPTH_BUFFER_BIT,
        temp.viewport_width,
        temp.viewport_height);
    }
  }

  // merge the result into f1
  {
    glBindFramebuffer(GL_FRAMEBUFFER, temp.f1.frame_buffer_handle);

    glUseProgram(temp.merge_gpu_program);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, temp.f3.color_component_handle);

    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_SRC_ALPHA);

    // draw fullscreen quad
    glDrawArrays(GL_TRIANGLES, 0, 3);

    glBlendFunc(GL_ONE, GL_ZERO);
    glDisable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
  }

  //and blit it into default
  {
    common::blit(
      temp.f1.frame_buffer_handle, 0,
      GL_COLOR_BUFFER_BIT,
      temp.viewport_width,
      temp.viewport_height);
  }
}

//============================================================================//
void on_resize_screen(temp_data& temp, int width, int height) noexcept
{
  delete_framebuffers(temp);

  temp.viewport_width  = width;
  temp.viewport_height = height;

  common::generate_framebuffer(temp.f1, width, height, false);
  common::generate_framebuffer(temp.f2, width, height, false);
  common::generate_framebuffer(temp.f3, width, height, false);
}

}
