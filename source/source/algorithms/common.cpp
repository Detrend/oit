//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#include <algorithms/common.h>

#include <camera.h>     // camera_s
#include <geometry.h>   // vertex_data

#include <GLAD/glad.h>

#include <array>        // std::array
#include <algorithm>    // std::contains

namespace oit::algorithms::common {

//============================================================================//
void delete_framebuffer(common::framebuffer& f)
{
  if (f.frame_buffer_handle)
  {
    glDeleteFramebuffers(1, &f.frame_buffer_handle);
    f.frame_buffer_handle = 0;
  }

  if (f.color_component_handle)
  {
    glDeleteTextures(1, &f.color_component_handle);
    f.color_component_handle = 0;
  }

  if (f.depth_component_handle)
  {
    if (glIsRenderbuffer(f.depth_component_handle))
    {
      glDeleteRenderbuffers(1, &f.depth_component_handle);
    }

    if (glIsTexture(f.depth_component_handle))
    {
      glDeleteTextures(1, &f.depth_component_handle);
    }

    f.depth_component_handle = 0;
  }
}

//============================================================================//
void delete_framebuffer(common::accumplus_framebuffer& f)
{
  if (f.frame_buffer_handle)
  {
    glDeleteFramebuffers(1, &f.frame_buffer_handle);
    f.frame_buffer_handle = 0;
  }

  for (u8 i = 0; i < f.num_color_components; ++i)
  {
    if (f.color_components[i])
    {
      glDeleteTextures(1, &f.color_components[i]);
      f.color_components[i] = 0;
    }
  }

  if (f.depth_component_handle)
  {
    if (glIsRenderbuffer(f.depth_component_handle))
    {
      glDeleteRenderbuffers(1, &f.depth_component_handle);
    }

    if (glIsTexture(f.depth_component_handle))
    {
      glDeleteTextures(1, &f.depth_component_handle);
    }

    f.depth_component_handle = 0;
  }
}

//============================================================================//
void generate_framebuffer(common::framebuffer& data, int width, int height, bool renderbuffer, GLint int_format)
{
  OIT_ASSERT(int_format == GL_RGBA || int_format == GL_RGBA16F || int_format == GL_RGBA32F);

  glGenFramebuffers(1, &data.frame_buffer_handle);
  glBindFramebuffer(GL_FRAMEBUFFER, data.frame_buffer_handle);

  // color texture (RGB8)
  glGenTextures(1, &data.color_component_handle);
  glBindTexture(GL_TEXTURE_2D, data.color_component_handle);
  glTexImage2D(
    GL_TEXTURE_2D, 0, int_format, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glBindTexture(GL_TEXTURE_2D, 0);

  // attach the texture to framebuffer
  glFramebufferTexture2D(
    GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, data.color_component_handle, 0);

  // use render buffer instead of depth buffer
  if (renderbuffer)
  {
    glGenRenderbuffers(1, &data.depth_component_handle);
    glBindRenderbuffer(GL_RENDERBUFFER, data.depth_component_handle);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glFramebufferRenderbuffer(
      GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, data.depth_component_handle);
  }
  else
  {
    glGenTextures(1, &data.depth_component_handle);
    glBindTexture(GL_TEXTURE_2D, data.depth_component_handle);
    glTexImage2D(
      GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, width, height, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glBindTexture(GL_TEXTURE_2D, 0);

    glFramebufferTexture2D(
      GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, data.depth_component_handle, 0);
  }

  OIT_ASSERT(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

//============================================================================//
void generate_framebuffer(
  common::accumplus_framebuffer& data,
  int                            width,
  int                            height,
  u8                             num_color_components,
  GLenum                         color_format,
  bool                           renderbuffer)
{
  OIT_ASSERT(num_color_components);
  OIT_ASSERT(num_color_components <= 8);
  OIT_ASSERT(color_format == GL_RGBA32F);

  glGenFramebuffers(1, &data.frame_buffer_handle);
  glBindFramebuffer(GL_FRAMEBUFFER, data.frame_buffer_handle);

  // generate all the textures
  glGenTextures(num_color_components, data.color_components);
  data.num_color_components = num_color_components;

  for (u8 i = 0; i < num_color_components; ++i)
  {
    glBindTexture(GL_TEXTURE_2D, data.color_components[i]);
    glTexImage2D(
      GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);

    // attach the texture to framebuffer
    glFramebufferTexture2D(
      GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0+i, GL_TEXTURE_2D, data.color_components[i], 0);
  }

  // use render buffer instead of depth buffer
  if (renderbuffer)
  {
    glGenRenderbuffers(1, &data.depth_component_handle);
    glBindRenderbuffer(GL_RENDERBUFFER, data.depth_component_handle);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glFramebufferRenderbuffer(
      GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, data.depth_component_handle);
  }
  else
  {
    glGenTextures(1, &data.depth_component_handle);
    glBindTexture(GL_TEXTURE_2D, data.depth_component_handle);
    glTexImage2D(
      GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, width, height, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glBindTexture(GL_TEXTURE_2D, 0);

    glFramebufferTexture2D(
      GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, data.depth_component_handle, 0);
  }

  OIT_ASSERT(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

//============================================================================//
void render_scene(
  const render_data&           data,
  std::span<oit::render_scene> scenes,
  obj_flags                    flags_present) noexcept
{
  glm::mat4 world_to_view   = calculate_camera_view(data.camera);
  glm::mat4 view_to_screen  = data.projection_matrix;
  glm::mat4 world_to_screen = view_to_screen * world_to_view;

  glUniformMatrix4fv(
    common::uniforms::world_to_view,
    1, false, glm::value_ptr(world_to_view));

  glUniformMatrix4fv(
    common::uniforms::view_to_screen,
    1, false, glm::value_ptr(view_to_screen));

  for (const auto& scene : scenes)
  {
    auto scene_transform = calculate_render_scene_transform(scene);

    // color override
    glUniform4f(
      common::uniforms::color_override,
      scene.color_override.r,
      scene.color_override.g,
      scene.color_override.b,
      scene.color_override.a);

    for (const auto& render_object : scene.objects)
    {
      if (render_object.texture_handle == 0)
      {
        continue;
      }

      auto flags = render_object.flags;

      // if the objects is forced to be transparent
      if (scene.color_override.a < 1.0f)
      {
        flags |= object_flags::transparent; // make it transparent
        flags &= ~object_flags::opaque;     // it is not opaque anymore
      }

      if (!(flags & flags_present))
      {
        continue;
      }

      glm::mat4 local_to_world = (flags & object_flags::particle)
        ? scene_transform * calculate_particle_transform(render_object, data.camera.rotation)
        : scene_transform * calculate_render_object_transform(render_object);

      glUniformMatrix4fv(
        common::uniforms::local_to_world,
        1, false, glm::value_ptr(local_to_world));

      glm::mat4 local_to_screen = world_to_screen * local_to_world;
      glUniformMatrix4fv(
        common::uniforms::local_to_screen,
        1, false, glm::value_ptr(local_to_screen));

      glBindBuffer(GL_ARRAY_BUFFER, render_object.vertex_buffer_handle);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, render_object.index_buffer_handle);
      glBindTexture(GL_TEXTURE_2D, render_object.texture_handle);

      // position (glm::vec3)
      glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex_data), nullptr);
      glEnableVertexAttribArray(0);

      // texture coords (glm::vec2)
      glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(vertex_data), (void*)sizeof(glm::vec3));
      glEnableVertexAttribArray(1);

      glDrawElements(GL_TRIANGLES, render_object.index_count, GL_UNSIGNED_INT, 0);
    }
  }
}

//============================================================================//
void clear_color_and_depth(glm::vec4 background_color)
{ 
  glClearColor(
    background_color.x,
    background_color.y,
    background_color.z,
    background_color.w);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

//============================================================================//
void delete_accum(accumulator_framebuffer& f)
{
  if (f.frame_buffer_handle)
  {
    glDeleteFramebuffers(1,  &f.frame_buffer_handle);
    glDeleteTextures(1,      &f.single_component_handle);
    glDeleteTextures(1,      &f.quad_component_handle);
    glDeleteRenderbuffers(1, &f.depth_component_handle);

    f.frame_buffer_handle     = 0;
    f.single_component_handle = 0;
    f.quad_component_handle   = 0;
    f.depth_component_handle  = 0;
  }
}

//============================================================================//
void generate_accum(accumulator_framebuffer& data, int width, int height, GLint single, GLint quad)
{
  auto SINGLE_SUPPORTED = std::array{GL_R8,    GL_R16F,    GL_R32F};
  auto QUAD_SUPPORTED   = std::array{GL_RGBA8, GL_RGBA16F, GL_RGBA32F};

  // check if single is valid format
  OIT_ASSERT(std::find(
    SINGLE_SUPPORTED.begin(),
    SINGLE_SUPPORTED.end(),
    single) != SINGLE_SUPPORTED.end(), "Non supported single format");

  // check if quad is a valid format
  OIT_ASSERT(std::find(
    QUAD_SUPPORTED.begin(),
    QUAD_SUPPORTED.end(),
    quad) != QUAD_SUPPORTED.end(), "Non supported quad format");

  glGenFramebuffers(1, &data.frame_buffer_handle);
  glBindFramebuffer(GL_FRAMEBUFFER, data.frame_buffer_handle);

  // counter texture (GL_R32F)
  {
    glGenTextures(1, &data.single_component_handle);
    glBindTexture(GL_TEXTURE_2D, data.single_component_handle);
    glTexImage2D(
      GL_TEXTURE_2D, 0, single, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    // attach the counter texture to color buffer
    glFramebufferTexture2D(
      GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, data.single_component_handle, 0);
  }

  // accumulator texture (RGBA32F)
  {
    glGenTextures(1, &data.quad_component_handle);
    glBindTexture(GL_TEXTURE_2D, data.quad_component_handle);
    glTexImage2D(
      GL_TEXTURE_2D, 0, quad, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    // attach the counter texture to color buffer
    glFramebufferTexture2D(
      GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, data.quad_component_handle, 0);

    // unbind
    glBindTexture(GL_TEXTURE_2D, 0);
  }

  // use render buffer instead of depth buffer
  {
    glGenRenderbuffers(1, &data.depth_component_handle);
    glBindRenderbuffer(GL_RENDERBUFFER, data.depth_component_handle);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    glFramebufferRenderbuffer(
      GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, data.depth_component_handle);
  }

  OIT_ASSERT(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

//============================================================================//
void blit(GLuint from, GLuint to, GLbitfield mask, int width, int height)
{
  glBindFramebuffer(GL_READ_FRAMEBUFFER, from);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, to);

  //glReadBuffer(GL_COLOR_ATTACHMENT0);
  glBlitFramebuffer(
    0, 0, width, height,
    0, 0, width, height,
    mask, GL_NEAREST);

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

}

