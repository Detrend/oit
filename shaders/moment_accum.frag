#version 430 core

layout(location = 42) uniform sampler2D texture_sampler;

layout(location = 43) uniform sampler2D moments_sum_sampler;
layout(location = 44) uniform sampler2D moments_sampler;

layout(location = 69) uniform vec4  color_override;
layout(location = 71) uniform vec4  near_far;

in vec2 texture_coords;
in vec3 view_coords;
in vec2 screen_coords;

// render targets
layout(location = 0) out vec4 coverage;
layout(location = 1) out vec4 accumulator;

// converts depth to unit
float depth_to_unit(float z)
{
    const float c0 = 1.0f / near_far.x;
    const float c1 = 1.0f / log(near_far.y / near_far.x);
    return log(z * c0) * c1;
}

float mad(float a, float b, float c)
{
    return a * b + c;
}

// copied from the MSM Shadow Mapping source paper
float Compute4MomentUnboundedShadowIntensity(vec4 Biased4Moments, float FragmentDepth, float DepthBias)
{
    float OutShadowIntensity = 0.0f;

    // Use short-hands for the many formulae to come
    vec4 b=Biased4Moments;
    vec3 z;
    z[0]=FragmentDepth-DepthBias;

    // Compute a Cholesky factorization of the Hankel matrix B storing only non-
    // trivial entries or related products
    float L21D11=mad(-b[0],b[1],b[2]);
    float D11=mad(-b[0],b[0], b[1]);
    float SquaredDepthVariance=mad(-b[1],b[1], b[3]);
    float D22D11=dot(vec2(SquaredDepthVariance,-L21D11), vec2(D11,L21D11));
    float InvD11=1.0f/D11;
    float L21=L21D11*InvD11;
    float D22=D22D11*InvD11;
    float InvD22=1.0f/D22;

    // Obtain a scaled inverse image of bz=(1,z[0],z[0]*z[0])^T
    vec3 c = vec3(1.0f,z[0],z[0]*z[0]);
    // Forward substitution to solve L*c1=bz
    c[1]-=b.x;
    c[2]-=b.y+L21*c[1];
    // Scaling to solve D*c2=c1
    c[1]*=InvD11;
    c[2]*=InvD22;
    // Backward substitution to solve L^T*c3=c2
    c[1]-=L21*c[2];
    c[0]-=dot(c.yz,b.xy);
    // Solve the quadratic equation c[0]+c[1]*z+c[2]*z^2 to obtain solutions 
    // z[1] and z[2]
    float InvC2=1.0f/c[2];
    float p=c[1]*InvC2;
    float q=c[0]*InvC2;
    float D=(p*p*0.25f)-q;
    float r=sqrt(D);
    z[1]=-p*0.5f-r;
    z[2]=-p*0.5f+r;
    // Compute the shadow intensity by summing the appropriate weights
    vec4 Switch=
        (z[2]<z[0]) ? vec4(z[1],z[0],1.0f,1.0f):(
        (z[1]<z[0]) ? vec4(z[0],z[1],0.0f,1.0f):
        vec4(0.0f,0.0f,0.0f,0.0f));

    float Quotient=(Switch[0]*z[2]-b[0]*(Switch[0]+z[2])+b[1])/((z[2]-Switch[1])*(z[0]-z[1]));
    OutShadowIntensity=Switch[2]+Switch[3]*Quotient;

    return clamp(OutShadowIntensity, 0.0f, 1.0f);
}

// calls the Hamburger4MSM function from the source paper
float hamburger_4_msm(vec4 moments, float z)
{
    moments = mix(moments, vec4(0.0, 0.375, 0.0, 0.375), 3.0e-7);
    return Compute4MomentUnboundedShadowIntensity(moments, z, 0.0f);
}

// calculates the weight of a pixel with this alpha
float calculate_weight_at_z(float alpha)
{
    vec4  b  = texelFetch(moments_sampler,     ivec2(gl_FragCoord.xy), 0);
    float b0 = texelFetch(moments_sum_sampler, ivec2(gl_FragCoord.xy), 0).r;
    float zf = gl_FragCoord.z;

    if (b0 != 0.0f)
        b = b / b0;

    return exp(-hamburger_4_msm(b, depth_to_unit(zf)) * b0);
}

void main()
{
    // fetch the texture
    vec4 tex = texture(texture_sampler, texture_coords);
    tex *= color_override;

    if (tex.a >= 1.0f)
    {
        discard;
    }

    // calculate weight
    float weight = calculate_weight_at_z(tex.a);

    coverage    = vec4(tex.a);
    accumulator = vec4(tex.rgb * tex.a * weight, tex.a * weight);
}
