#version 430 core

out vec4 FragColor;

layout(location = 42) uniform sampler2D accumulate_texture;
layout(location = 43) uniform sampler2D coverage_texture;

void main()
{
    ivec2 coord = ivec2(gl_FragCoord.xy);
    vec4  accum = texelFetch(accumulate_texture, coord, 0);
    float r     = texelFetch(coverage_texture, coord, 0).x;

    FragColor = vec4(accum.rgb / max(accum.a, 0.0001f), r);
}