#version 430 core
out vec4 FragColor;

// uniforms
layout(location = 69) uniform vec4 color_override;

uniform sampler2D texture_sampler;

in vec2 texture_coords;

void main()
{
    vec4 color = texture(texture_sampler, texture_coords);
    color *= color_override;

    if (color.a < 1.0f)
    {
        discard;
    }

    FragColor = color;
} 