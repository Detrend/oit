#version 430 core

layout(location = 42) uniform sampler2D texture_sampler;

layout(location = 69) uniform vec4  color_override;
layout(location = 70) uniform ivec4 weight_method;
layout(location = 71) uniform vec4  near_far;
layout(location = 80) uniform vec4  scale_near_far;

layout(location = 99) uniform sampler2D depth_sampler;

in vec2 texture_coords;

layout(location = 0) out vec4 coverage;
layout(location = 1) out vec4 accumulator;

float depth_to_unit(float z)
{
    const float c0 = 1.0f / near_far.x;
    const float c1 = 1.0f / log(near_far.y / near_far.x);
    return log(z * c0) * c1;
}

float depth_to_linear_world(float z)
{
    float near = near_far.x;
    float far  = near_far.y;
    return near * far / (far + z * (near - far));
}

// calculate weights for a given pixel in depth z with an alpha alpha
float calculate_weights(float z, float alpha)
{
    float near = near_far.x;
    float far  = near_far.y;

    float snear = scale_near_far.x;
    float sfar  = scale_near_far.y;

    float sd  = texelFetch(depth_sampler, ivec2(gl_FragCoord.xy), 0).x;
    float sdu = (clamp(depth_to_linear_world(sd), near, far) - near) / (far - near);

    float unit  = depth_to_unit(z);
    float world = depth_to_linear_world(z);
    float wu   = (world - near) / (far - near);
    float wu2  = wu * wu;
    float wsu  = clamp(wu / sdu, 0.0f, 1.0f);
    float wsu2 = wsu * wsu;

    float world_scaled = mix(snear, sfar, wu);
    const float lower_clamp = 0.000000000001f;

    switch (weight_method.x)
    {
        case 1:   return 1.0f / max(wu2 * wu, lower_clamp);
        case 2:   return 1.0f / max(wu2 * wu2 * wu, lower_clamp);
        case 3:   return 1.0f / max(wu2 * wu, lower_clamp)       * alpha;
        case 4:   return 1.0f / max(wu2 * wu2 * wu, lower_clamp) * alpha;
        case 5:   return alpha * clamp(10.0f / (1.0f/100000.0f + pow(world_scaled * 0.2f, 2) + pow(world_scaled / 200.0f, 6)), 0.01f, 3000.0f);
        case 6:   return alpha * clamp(10.0f / (1.0f/100000.0f + pow(world_scaled * 0.1f, 3) + pow(world_scaled / 200.0f, 6)), 0.01f, 3000.0f);
        case 7:   return alpha * clamp(1.0f, 0.01f, 3000.0f);
        case 8:   return 1.0f / max(wsu2 * wsu,        lower_clamp);
        case 9:   return 1.0f / max(wsu2 * wsu2 * wsu, lower_clamp);
        case 10:  return 1.0f / max(wsu2 * wsu,        lower_clamp) * alpha;
        case 11:  return 1.0f / max(wsu2 * wsu2 * wsu, lower_clamp) * alpha;
        case 12:  return 1.0f / max(wsu2 * wsu,        lower_clamp) * alpha * alpha;
        case 13:  return 1.0f / max(wsu2 * wsu2 * wsu, lower_clamp) * alpha * alpha;
        default:  return 1.0f;
    }

}

void main()
{
    vec4 tex = texture(texture_sampler, texture_coords);
    tex *= color_override;

    if (tex.a >= 1.0f)
    {
        discard;
    }

    float weight = calculate_weights(gl_FragCoord.z, tex.a);

    // write to multiple render targets
    coverage    = vec4(tex.a);
    accumulator = vec4(tex.rgb * tex.a * weight, tex.a * weight);
}