#version 430 core
out vec4 FragColor;

// uniforms
layout(location = 69) uniform vec4 color_override;
layout(location = 71) uniform vec4 near_far;

layout(location = 301) uniform ivec4 bucket_count_dist;
layout(location = 409) uniform ivec4 debug_index;

uniform sampler2D texture_sampler;

in vec2 texture_coords;

float depth_to_linear_unit(float z)
{
    float near = near_far.x;
    float far  = near_far.y;
    return ((near * far / (far + z * (near - far))) - near) / (far - near);
}

void calculate_bucket_index(out int index, out float factor)
{
    const int bucket_count = bucket_count_dist.x;
    const int bucket_pow   = int(pow(2, bucket_count));

    float unit_z = depth_to_linear_unit(gl_FragCoord.z);
    float norm   = clamp(log2(unit_z * float(bucket_pow) + 1.0f), 0.0f, bucket_count * 0.999999f);
    index  = clamp(int(norm), 0, bucket_count - 1);
    factor = mod(norm, 1.0f);
}

void main()
{
    vec4 color = texture(texture_sampler, texture_coords);
    color *= color_override;

    if (debug_index.x != 0)
    {
        int bucket = 0;
        float factor = 0.0f;
        calculate_bucket_index(bucket, factor);
        color = (int(mod(bucket, 2))) == 0 ? vec4(color.r * factor, 0.0f, 0.0f, 1.0f) : vec4(0.0f, color.g * factor, 0.0f, 1.0f);
    }

    if (color.a < 1.0f)
    {
        discard;
    }

    FragColor = color;
} 