#version 430 core

out vec4 FragColor;

layout(location = 42) uniform sampler2D accumulate_texture;
layout(location = 43) uniform sampler2D coverage_texture;

layout(location = 201) uniform sampler2D buckets1;
layout(location = 202) uniform sampler2D buckets2;
layout(location = 203) uniform sampler2D buckets3;
layout(location = 204) uniform sampler2D buckets4;

layout(location = 71) uniform vec4  near_far;

layout(location = 409) uniform ivec4 debug_bucket_index;
layout(location = 301) uniform ivec4 bucket_count_dist;

float depth_to_linear_unit(float z)
{
    float near = near_far.x;
    float far  = near_far.y;
    return ((near * far / (far + z * (near - far))) - near) / (far - near);
}

void calculate_bucket_index(float z, out int index, out float factor)
{
    const int bucket_count = bucket_count_dist.x;
    const int bucket_pow   = int(pow(2, bucket_count));

    float unit_z = depth_to_linear_unit(z);
    float norm   = clamp(log2(unit_z * float(bucket_pow) + 1.0f), 0.0f, bucket_count * 0.999999f);
    index  = clamp(int(norm), 0, bucket_count - 1);
    factor = mod(norm, 1.0f);
}

float calculate_weights(float z)
{
    ivec2 coords = ivec2(gl_FragCoord.xy);
    float unit_z = depth_to_linear_unit(z);

    vec4 b1 = texelFetch(buckets1, coords, 0);
    vec4 b2 = texelFetch(buckets2, coords, 0);
    vec4 b3 = texelFetch(buckets3, coords, 0);
    vec4 b4 = texelFetch(buckets4, coords, 0);

    int   bucket_index     = 0;
    float my_bucket_weight = 0.0f;
    calculate_bucket_index(z, bucket_index, my_bucket_weight);

    float coverage = 1.0f;

    // previous buckets
    for (int i = 0; i <= bucket_index; ++i)
    {
        int m = int(mod(i, 4));
        float cov = 1.0f;

        if (i < 4)
        {
            cov = b1[m];
        }
        else if (i < 8)
        {
            cov = b2[m];
        }
        else if (i < 12)
        {
            cov = b3[m];
        }
        else
        {
            cov = b4[m];
        }

        // coverage *= cov;
        if (i == bucket_index)
        {
            coverage *= mix(1.0f, cov, my_bucket_weight);
        }
        else
        {
            coverage *= cov;
        }
    }

    return coverage;
}

void main()
{
    ivec2 coord = ivec2(gl_FragCoord.xy);
    vec4  accum = texelFetch(accumulate_texture, coord, 0);
    float r     = texelFetch(coverage_texture, coord, 0).x;

    float coverage = calculate_weights(1.0f);

    FragColor = vec4(accum.rgb / max(accum.a, 0.0001f), r);
    // FragColor = vec4(mix(vec3(1.0f, 0.0f, 0.0f), vec3(0.0f, 0.0f, 1.0f), coverage), 0.0f);
}