#version 430 core

layout(location = 42) uniform sampler2D texture_sampler;

layout(location = 69) uniform vec4 color_override;

in vec2 texture_coords;

layout(location = 0) out vec4 counter;
layout(location = 1) out vec4 accumulator;

void main()
{
    vec4 tex = texture(texture_sampler, texture_coords);
    tex *= color_override;

    if (tex.a >= 1.0f)
    {
        discard;
    }

    accumulator = vec4(tex.rgb * tex.a, tex.a);
    counter     = vec4(1.0f);
}