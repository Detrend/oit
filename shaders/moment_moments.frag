#version 430 core

layout(location = 42) uniform sampler2D texture_sampler;

layout(location = 69) uniform vec4  color_override;
layout(location = 71) uniform vec4  near_far;

in vec2 texture_coords;
in vec3 view_coords;

layout(location = 0) out vec4 moments_sum;
layout(location = 1) out vec4 moments;

// converts distance to unit depth
float depth_to_unit(float z)
{
    const float c0 = 1.0f / near_far.x;
    const float c1 = 1.0f / log(near_far.y / near_far.x);
    return log(z * c0) * c1;
}

// calculate moments and store them
void moment_pass_1()
{
    vec4 tex = texture(texture_sampler, texture_coords);    
    tex *= color_override;

    if (tex.a >= 1.0f)
    {
        discard;
    }

    const float kMaxAlpha = 1.0 - 0.5/256.0;
    float d = -log(1.0f - (tex.a * kMaxAlpha));

    float z  = depth_to_unit(gl_FragCoord.z);
    float z2 = z * z;
    float z3 = z2 * z;
    float z4 = z2 * z2;

    moments_sum = vec4(d, d, d, d);
    moments     = vec4(z, z2, z3, z4) * d;
}

void main()
{
    moment_pass_1();
}