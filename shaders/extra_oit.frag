#version 430 core

layout(location = 42) uniform sampler2D texture_sampler;

layout(location = 69) uniform vec4  color_override;
layout(location = 70) uniform ivec4 weight_method;
layout(location = 71) uniform vec4  near_far;

layout(location = 99)  uniform sampler2D depth_sampler;
layout(location = 201) uniform sampler2D buckets1;
layout(location = 202) uniform sampler2D buckets2;
layout(location = 203) uniform sampler2D buckets3;
layout(location = 204) uniform sampler2D buckets4;

layout(location = 409) uniform ivec4 debug_bucket_index;
layout(location = 301) uniform ivec4 bucket_count_dist;

in vec2 texture_coords;

layout(location = 0) out vec4 coverage;
layout(location = 1) out vec4 accumulator;

float depth_to_linear_unit(float z)
{
    float near = near_far.x;
    float far  = near_far.y;
    return ((near * far / (far + z * (near - far))) - near) / (far - near);
}

void calculate_bucket_index(out int index, out float factor)
{
    const int bucket_count = bucket_count_dist.x;
    const int bucket_pow   = int(pow(2, bucket_count));

    float unit_z = depth_to_linear_unit(gl_FragCoord.z);
    float norm   = clamp(log2(unit_z * float(bucket_pow) + 1.0f), 0.0f, bucket_count * 0.999999f);
    index  = clamp(int(norm), 0, bucket_count - 1);
    factor = mod(norm, 1.0f);
}

float fetch_bucket(vec4 b1, vec4 b2, vec4 b3, vec4 b4, int i)
{
    int m = int(mod(i, 4));
    float cov = 1.0f;

    if (i < 4)
    {
        cov = b1[m];
    }
    else if (i < 8)
    {
        cov = b2[m];
    }
    else if (i < 12)
    {
        cov = b3[m];
    }
    else
    {
        cov = b4[m];
    }

    return cov;
}

float calculate_weights()
{
    ivec2 coords = ivec2(gl_FragCoord.xy);

    float z      = gl_FragCoord.z;
    float unit_z = depth_to_linear_unit(z);

    vec4 b1 = texelFetch(buckets1, coords, 0);
    vec4 b2 = texelFetch(buckets2, coords, 0);
    vec4 b3 = texelFetch(buckets3, coords, 0);
    vec4 b4 = texelFetch(buckets4, coords, 0);

    int   bucket_index     = 0;
    float my_bucket_weight = 0.0f;
    calculate_bucket_index(bucket_index, my_bucket_weight);

    float coverage = 1.0f;

    // previous buckets
    for (int i = 0; i <= bucket_index; ++i)
    {
        float me = fetch_bucket(b1, b2, b3, b4, i);
        coverage *= me;
    }

    return coverage;
}

void main()
{
    vec4 tex = texture(texture_sampler, texture_coords);
    tex *= color_override;

    if (tex.a >= 1.0f)
    {
        discard;
    }

    float weight = calculate_weights();

    coverage    = vec4(tex.a);
    accumulator = vec4(tex.rgb * tex.a * weight, tex.a * weight);
}