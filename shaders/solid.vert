#version 430 core

// inputs
layout(location = 0) in vec3 aPos;
layout(location = 1) in vec2 aTexCoords;
layout(location = 2) in vec3 aColor;

// uniforms
layout(location = 0) uniform mat4 local_to_world;
layout(location = 1) uniform mat4 world_to_view;
layout(location = 2) uniform mat4 view_to_screen;

layout(location = 3) uniform mat4 local_to_screen;

out vec2 texture_coords;
out vec3 view_coords;
out vec2 screen_coords;

void main()
{
    texture_coords = aTexCoords;
    gl_Position = local_to_screen * vec4(aPos, 1.0);
    view_coords   = gl_Position.xyz;
    screen_coords = 0.5 * gl_Position.xy + vec2(0.5);
}