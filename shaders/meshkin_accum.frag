#version 430 core
out vec4 FragColor;

layout(location = 42) uniform sampler2D texture_sampler;
layout(location = 69) uniform vec4      color_override;

in vec2 texture_coords;

void main()
{
    vec4 tex = texture(texture_sampler, texture_coords);
    tex *= color_override;

    if (tex.a >= 1.0f)
    {
        discard;
    }

    FragColor = vec4(tex.rgb * tex.a, tex.a);
} 