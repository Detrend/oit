#version 430 core

out vec4 FragColor;

layout(location = 42) uniform sampler2D accumulate_texture;
layout(location = 43) uniform sampler2D count_texture;

void main()
{
    vec4  accum = texelFetch(accumulate_texture, ivec2(gl_FragCoord.xy), 0);
    float n     = max(texelFetch(count_texture, ivec2(gl_FragCoord.xy), 0).x, 1.0f);

    FragColor = vec4(accum.rgb / max(accum.a, 0.0001f), pow(max(0.0f, 1.0f - accum.a / n), n));
} 