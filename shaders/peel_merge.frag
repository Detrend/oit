#version 430 core

out vec4 FragColor;

layout(location = 99) uniform sampler2D transparent_layer;

in vec2 screen_coords;

void main()
{
    // full screen pass for merging the accumulated color
    vec4 col = texelFetch(transparent_layer, ivec2(gl_FragCoord.xy), 0);
    FragColor = col;
}
