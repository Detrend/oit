#version 430 core
out vec4 FragColor;

layout(location = 43) uniform sampler2D accumulator_texture;
layout(location = 44) uniform sampler2D background_texture;

void main()
{
    vec4 accum = texelFetch(accumulator_texture, ivec2(gl_FragCoord.xy), 0);
    vec3 back  = texelFetch(background_texture,  ivec2(gl_FragCoord.xy), 0).rgb;

    FragColor = vec4(accum.rgb + back * (1.0f - accum.a), 1.0f);
} 