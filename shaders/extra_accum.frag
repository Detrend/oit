#version 430 core

layout(location = 42) uniform sampler2D texture_sampler;
layout(location = 69) uniform vec4      color_override;
layout(location = 71) uniform vec4      near_far;
layout(location = 99) uniform sampler2D depth_sampler;

layout(location = 409) uniform ivec4 debug_bucket_index;
layout(location = 301) uniform ivec4 bucket_count_dist;

in vec2 texture_coords;

layout(location = 0) out vec4 coverage1;
layout(location = 1) out vec4 coverage2;
layout(location = 2) out vec4 coverage3;
layout(location = 3) out vec4 coverage4;

float depth_to_linear_unit(float z)
{
    float near = near_far.x;
    float far  = near_far.y;
    return ((near * far / (far + z * (near - far))) - near) / (far - near);
}

void calculate_bucket_index(out int index, out float factor)
{
    const int bucket_count = bucket_count_dist.x;
    const int bucket_pow   = int(pow(2, bucket_count));

    float unit_z = depth_to_linear_unit(gl_FragCoord.z);
    float norm   = clamp(log2(unit_z * float(bucket_pow) + 1.0f), 0.0f, bucket_count * 0.999999f);
    index  = clamp(int(norm), 0, bucket_count - 1);
    factor = mod(norm, 1.0f);
}

void calculate_buckets(float z, float max_z, float a, out vec4 b1, out vec4 b2, out vec4 b3, out vec4 b4)
{
    b1 = vec4(1.0f);
    b2 = vec4(1.0f);
    b3 = vec4(1.0f);
    b4 = vec4(1.0f);

    int bucket_index = 0;
    float fraction   = 0.0f;
    calculate_bucket_index(bucket_index, fraction);

    int m = int(mod(bucket_index, 4));
    if (bucket_index < 4)
    {
        b1[m] = 1.0f - a;
    }
    else if (bucket_index < 8)
    {
        b2[m] = 1.0f - a;
    }
    else if (bucket_index < 12)
    {
        b3[m] = 1.0f - a;
    }
    else
    {
        b4[m] = 1.0f - a;
    }
}

void main()
{
    float alpha = texture(texture_sampler, texture_coords).a;
    alpha *= color_override.a;

    if (alpha >= 1.0f)
    {
        discard;
    }

    float max_z = texelFetch(depth_sampler, ivec2(gl_FragCoord.xy), 0).x;
    float z     = gl_FragCoord.z;

    vec4 bucket1 = vec4(1.0f);
    vec4 bucket2 = vec4(1.0f);
    vec4 bucket3 = vec4(1.0f);
    vec4 bucket4 = vec4(1.0f);

    calculate_buckets(z, max_z, alpha, bucket1, bucket2, bucket3, bucket4);

    coverage1 = bucket1;
    coverage2 = bucket2;
    coverage3 = bucket3;
    coverage4 = bucket4;
}