#version 430 core

out vec4 FragColor;

layout(location = 42) uniform sampler2D texture_sampler;
layout(location = 51) uniform sampler2D additional_depth;

layout(location = 69) uniform vec4 color_override;

in vec2 screen_coords;
in vec2 texture_coords;

void main()
{
    // manual depth test
    float depth = texelFetch(additional_depth, ivec2(gl_FragCoord.xy), 0).r;
    if (gl_FragCoord.z <= depth)
    {
        discard;
    }

    vec4 tex = texture(texture_sampler, texture_coords);
    tex *= color_override;
    if (tex.a >= 1.0f)
    {
        discard;
    }

    // calculate the final color
    FragColor = vec4(tex.rgb * tex.a, tex.a);
}
