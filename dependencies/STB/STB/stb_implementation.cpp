//============================================================================//
// Author: Matus Rozek                                                        //
// Year: 2024                                                                 //
// Program written as a part of a bachelor thesis for MFF Cuni                //
//============================================================================//
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

static bool stbi_settings = []()->bool
{
  // flip the ordering of the pixels
  stbi_set_flip_vertically_on_load(true);
  return false;
}();
